"use strict"

import gulp from "gulp"
import { spawn } from "child_process"
import fs from "fs"
import colors from "ansi-colors"
import log from "fancy-log"
import yaml from "js-yaml"
import backstop from "backstopjs"
import _ from "lodash"
import yargs from "yargs"
import rename from "gulp-rename"

// Config Setup
const { SLUG, PATHS, WPQUERY, BACKSTOP } = loadConfig()

function loadConfig() {
  const filePath = "gulp-config.yml"
  const encoding = "utf8"

  try {
    let ymlFile = fs.readFileSync(filePath, encoding)
    const config = yaml.load(ymlFile)

    if (!config || !config.SLUG) {
      throw new Error(`SLUG must be defined in '${__dirname}/${filePath}'`)
    }

    return config
  } catch (e) {
    if (e.code === "ENOENT") {
      const file = fs.readFileSync("gulp-config.example.yml", encoding)
      fs.writeFileSync(filePath, encoding)
      log(
        `${colors.bgcyan(`[Info]: You need to setup a '${filePath}' file.`)}
            - Luckily, we have created one for you!
            - Make sure to edit the ${colors.bold(
              `SLUG`
            )} property to match your Lando slug!
        `
      )
      process.exit(1)
    } else {
      log(colors.bold(colors.red(e)))
      process.exit(1)
    }
  }
}

// Expose Tasks to CLI
//
// Backstop
gulp.task("backstop:pull", getPublishedPosts)
gulp.task("backstop:reference", captureBackstopReference)
gulp.task("backstop:test", runBackstopTests)
gulp.task(
  "backstop",
  gulp.series("backstop:pull", "backstop:reference", "backstop:test")
)
// Init
gulp.task("init:pantheon", function(done) {
  fs.copyFile(
    "pantheon.example.yml",
    "pantheon.yml",
    fs.constants.COPYFILE_EXCL,
    err => {
      if (err) done(err)
      done()
    }
  )
})
gulp.task("init", gulp.parallel("init:pantheon"))

// Configure Tasks
function getPublishedPosts(done) {
  const filePath = PATHS.backstop.postList

  const cmd = "terminus"
  const args = _([
    "remote:wp",
    `${SLUG}.live`,
    "--",
    "post",
    "list",
    "--format=json"
  ])
    .concat(WPQUERY.type ? `--post_type=${WPQUERY.type.join(",")}` : [])
    .concat(WPQUERY.fields ? `--fields=${WPQUERY.fields.join(",")}` : [])
    .concat(WPQUERY.status ? `--post_status=${WPQUERY.status}` : [])
    .compact()
    .value()

  const task = spawn(cmd, args, { stdio: ["inherit", "pipe", "pipe"] })

  fs.unlink(filePath, err => {
    const stream = fs.createWriteStream(filePath)

    log(colors.dim(`Running: ${_.concat(cmd, args).join(" ")}`))

    task.stdout.on("data", data => {
      try {
        JSON.parse(data)
        stream.write(data)
      } catch (e) {
        log(colors.dim(`JSON Warning: ${e}`))
      }
    })

    task.stderr.on("data", data => {
      log(colors.dim(`Terminus: ${data}`))
    })
  })

  return task
}

function captureBackstopReference() {
  const config = getBackstopConfig()
  return backstop("reference", { config })
}

function runBackstopTests() {
  const config = getBackstopConfig()
  return backstop("test", { config })
}

function getBackstopConfig() {
  const { scenarioDefaults, prefixes, domains, paths, ...rest } = BACKSTOP
  const MAX_FILENAME_LENGTH = 255

  const posts = require(`./${PATHS.backstop.postList}`)

  return {
    id: SLUG,
    ...rest,
    paths: paths.reduce((prev, curr) => {
      prev[curr] = `${PATHS.backstop.data}/${curr}`
      return prev
    }, {}),
    scenarios: posts
      .filter(item => !!item.post_name)
      .filter(({ url }) => url.length < MAX_FILENAME_LENGTH / 2 - 15)
      .map(item => {
        const { post_name, url } = item

        const relativeUrl = url.replace(/^https?:\/\/.*?(?=\/)/gi, "")

        return {
          ...scenarioDefaults,
          label: `${post_name} (${url})`,
          url: `https://${prefixes.test}${SLUG}.${domains.test}${relativeUrl}`,
          referenceUrl: `https://${prefixes.reference}${SLUG}.${
            domains.reference
          }${relativeUrl}`
        }
      })
  }
}
