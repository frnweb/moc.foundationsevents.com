<?php if(!class_exists('WSAL_Rep_Plugin')){ exit('You are not allowed to view this page.'); }
/**
 * Class WSAL_Rep_HtmlReportGenerator
 * Provides utility methods to generate an html report
 */
class WSAL_Rep_HtmlReportGenerator
{
    private $_filters = array();

    protected $date_format = null;
    protected $gmt_offset = 0;

    public function __construct($date_format, $gmt_offset)
    {
        $this->date_format = $date_format;
        $this->gmt_offset = $gmt_offset;
    }

    /**
     * Generate the HTML report file
     * @param array $data
     * @param array $filters
     * @param string $uploadsDirPath
     * @param array , $_alertGroups
     * @return int|string
     */
    public function Generate(array $data, array $filters, $uploadsDirPath, array $_alertGroups = array())
    {
        if (empty($data)) {
            return 0;
        }
        if (empty($filters)) {
            return 0;
        }
        $this->_filters = $filters;

        // Split data by blog so we can display an organized report
        $tempData = array();
        foreach ($data as $k => $entry) {
            $blogName = $entry['blog_name'];
            if (!isset($tempData[$blogName])) {
                $tempData[$blogName] = array();
            }
            array_push($tempData[$blogName], $entry);
        }

        if (empty($tempData)) {
            return 0;
        }

        // Check directory once more
        if (!is_dir($uploadsDirPath) || !is_readable($uploadsDirPath) || !is_writable($uploadsDirPath)) {
            return 1;
        }

        $fn = 'wsal_report_'.WSAL_Rep_Util_S::GenerateRandomString().'.html';
        $fp = $uploadsDirPath.$fn;

        $file = fopen($fp, 'w');

        fwrite($file, '<!DOCTYPE html><html><head>');
        fwrite($file, '<meta charset="utf-8">');
        fwrite($file, '<title>'.__('WP Security Audit Log Reporter', 'reports-wsal').'</title>');
        //fwrite($file, $this->generate_styles());
        fwrite($file, '</head>');
        fwrite($file, '<body style=\'margin: 0 0;padding: 0 0;font-family: "Open Sans", sans-serif;font-size: 14px;color: #404040;\'><div class="wsal_report_wrap" style="margin: 20px 25px;">');
        fwrite($file, $this->_writeHeader(array_keys($tempData), $_alertGroups));

        foreach ($tempData as $blogName => $alerts) {
            $this->_writeAlertsForBlog($blogName, $alerts, $file);
        }

        fwrite($file, '</div></body></html>');
        fclose($file);
        return $fn;
    }

    private function _writeHeader(array $blogNames, $_alertGroups)
    {
        $str = '<p id="by" style="font-size: 13px; margin: 0 0; padding: 0 0; text-align: center;">Report generated with</p>';
        $str .= '<div id="section-1" style="margin: 0 0; padding: 0 0;text-align: center;">';
        $str .= '<a href="http://www.wpwhitesecurity.com" target="_blank" style="text-decoration:none;"><h1 style="color: rgb(54,95, 145);">WP Security Audit Log</h1></a>';
        $str .= '<p id="dev" style="margin-top:10px;">WordPress Plugin</p>';
        $str .= '</div>';

        $t = time();
        $df = $this->date_format . ' h:i:s';
        $date = date($df, $t + $this->gmt_offset);

        $user = wp_get_current_user();
        $str .= '<div id="section-2" style="margin: 0 0; padding: 0 0;">';
        $str .= '<p style="margin: 0 0; padding: 5px 0; font-size: 13px;border-bottom: solid 1px #333;"><strong>'.__('Report Details:', 'reports-wsal').'</strong></p>';
        $str .= '<p style="margin: 0 0; padding: 5px 0; font-size: 13px;"><strong>'.__('Generated on:', 'reports-wsal').'</strong> '.$date.'</p>';
        $str .= '<p style="margin: 0 0; padding: 5px 0; font-size: 13px;"><strong>'.__('Generated by:', 'reports-wsal').'</strong> '.$user->user_login.'</p>';

        $filters = $this->_filters;

        $tHeader = '<table class="wsal_report_table" style="border: solid 1px #333333;border-spacing:5px;border-collapse: collapse;margin: 0 0;width: 100%;font-size: 14px;">';
        $tHeader .= '<thead style="background-color: #555555;border: 1px solid #555555;color: #ffffff;padding: 0 0;text-align: left;vertical-align: top;"><tr>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('Site(s)', 'reports-wsal').'</p></td>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('User(s)', 'reports-wsal').'</p></td>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('Role(s)', 'reports-wsal').'</p></td>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('IP address(es)', 'reports-wsal').'</p></td>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('Alert Groups', 'reports-wsal').'</p></td>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('Alert Code(s)', 'reports-wsal').'</p></td>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('Start date', 'reports-wsal').'</p></td>';
        $tHeader .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('End date', 'reports-wsal').'</p></td>';
        $tHeader .= '</tr></thead>';

        $v1 =
        $v2 =
        $v3 =
        $v4 =
        $v5 =
        $v6 = __('All', 'reports-wsal');
        $v7 = __('From the beginning', 'reports-wsal');
        $v8 = date($this->date_format);

        if (!empty($filters['sites'])) {
            $v1 = implode(', ', $blogNames);
        }
        if (!empty($filters['users'])) {
            $tmp = array();
            foreach ($filters['users'] as $userId) {
                $u = get_user_by('id', $userId);
                array_push($tmp, $u->user_login);
            }
            $v2 = implode(', ', $tmp);
        }
        if (!empty($filters['roles'])) {
            $v3 = implode(', ', $filters['roles']);
        }
        if (!empty($filters['ip-addresses'])) {
            $v4 = implode(', ', $filters['ip-addresses']);
        }
        if (! empty($filters['alert_codes']['groups'])) {
            if (count($_alertGroups) <> count($filters['alert_codes']['groups'])) {
                $v5 = implode(', ', $filters['alert_codes']['groups']);
            }
        }
        if (! empty($filters['alert_codes']['alerts'])) {
            $v6 = implode(', ', $filters['alert_codes']['alerts']);
        }
        if (! empty($filters['date_range']['start'])) {
            $v7 = date($this->date_format, strtotime($filters['date_range']['start']));
        }
        if (! empty($filters['date_range']['end'])) {
            $v8 = date($this->date_format, strtotime($filters['date_range']['end']));
        }

        $str .= '<p><strong>'.__('Criteria', 'reports-wsal').':</strong></p>';

        $tbody = '<tbody><tr>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v1.'</p></td>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v2.'</p></td>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v3.'</p></td>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v4.'</p></td>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v5.'</p></td>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v6.'</p></td>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v7.'</p></td>';
        $tbody .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v8.'</p></td>';
        $tbody .= '</tr></tbody>';
        $tbody .= '</table>';

        $str .= $tHeader.$tbody;

        $str .= '</div>';
        return $str;
    }

    private function _writeAlertsForBlog($blogName, array $data, $file)
    {
        fwrite($file, '<h3 style="font-size: 20px; margin: 25px 0;">'.$blogName.'</h3>');
        fwrite($file, '<table class="wsal_report_table" style="border: solid 1px #333333;border-spacing:5px;border-collapse: collapse;margin: 0 0;width: 100%;font-size: 14px;">');
        $columns = array(
            'Code',
            'Type',
            'Date',
            'Username',
            'Role',
            'Source IP',
            'Messsage'
        );
        $h = '';
        foreach ($columns as $item) {
            $h .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$item.'</p></td>';
        }
        fwrite($file, '<thead style="background-color: #555555;border: 1px solid #555555;color: #ffffff;padding: 0 0;text-align: left;vertical-align: top;"><tr>'.$h.'</tr></thead>');
        fwrite($file, '<tbody>');
        
        foreach ($data as $i => $alert) {
            $r = ($i % 2 != 0) ? '<tr style="background-color: #f1f1f1;">' : '<tr style="background-color: #ffffff;">';
            $r .= '<td style="padding: 16px 7px;"><p class="code" style="margin: 0;text-align: center; font-weight: 700;">'.$alert['alert_id'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$alert['code'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$alert['date'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$alert['user_name'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$alert['role'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$alert['user_ip'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$alert['message'].'</p></td>';

            $r .= '</tr>';
            fwrite($file, $r);
        }
        fwrite($file, '</tbody></table>');
    }

    public function GenerateUniqueIPS($data, $uploadsDirPath, $dateStart, $dateEnd)
    {
        if (empty($data)) {
            return 0;
        }
        // Check directory once more
        if (!is_dir($uploadsDirPath) || !is_readable($uploadsDirPath) || !is_writable($uploadsDirPath)) {
            return 1;
        }

        $fn = 'wsal_report_'.WSAL_Rep_Util_S::GenerateRandomString().'.html';
        $fp = $uploadsDirPath.$fn;
        $t = time();
        $df = $this->date_format . ' h:i:s';
        $date = date($df, $t + $this->gmt_offset);

        if (!empty($dateStart)) {
            $v1 = date($this->date_format, strtotime($dateStart));
        }
        if (!empty($dateEnd)) {
            $v2 = date($this->date_format, strtotime($dateEnd));
        }
        $user = wp_get_current_user();

        $file = fopen($fp, 'w');

        fwrite($file, '<!DOCTYPE html><html><head>');
        fwrite($file, '<meta charset="utf-8">');
        fwrite($file, '<title>'.__('WP Security Audit Log Reporter', 'reports-wsal').'</title>');
        fwrite($file, '</head>');
        fwrite($file, '<body style=\'margin: 0 0;padding: 0 0;font-family: "Open Sans", sans-serif;font-size: 14px;color: #404040;\'><div class="wsal_report_wrap" style="margin: 20px 25px;">');
        $html = '<p id="by" style="font-size: 13px; margin: 0 0; padding: 0 0; text-align: center;">Report generated with</p>';
        $html .= '<div id="section-1" style="margin: 0 0; padding: 0 0;text-align: center;">';
        $html .= '<a href="http://www.wpwhitesecurity.com" target="_blank" style="text-decoration:none;"><h1 style="color: rgb(54,95, 145);">WP Security Audit Log</h1></a>';
        $html .= '<p id="dev" style="margin-top:10px;">WordPress Plugin</p>';
        $html .= '</div>';
        $html .= '<div id="section-2" style="margin: 0 0; padding: 0 0;">';
        $html .= '<p style="margin: 0 0; padding: 5px 0; font-size: 13px;border-bottom: solid 1px #333;"><strong>'.__('Report Details:', 'reports-wsal').'</strong></p>';
        $html .= '<p style="margin: 0 0; padding: 5px 0; font-size: 13px;"><strong>'.__('Generated on:', 'reports-wsal').'</strong> '.$date.'</p>';
        $html .= '<p style="margin: 0 0; padding: 5px 0; font-size: 13px;"><strong>'.__('Generated by:', 'reports-wsal').'</strong> '.$user->user_login.'</p>';
        $html .= '<p><strong>'.__('Criteria', 'reports-wsal').':</strong></p>';
        $html .= '<table class="wsal_report_table" style="border: solid 1px #333333;border-spacing:5px;border-collapse: collapse;margin: 0 0;width: 100%;font-size: 14px;">';
        $html .= '<thead style="background-color: #555555;border: 1px solid #555555;color: #ffffff;padding: 0 0;text-align: left;vertical-align: top;"><tr>';
        $html .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('Alert Code(s)', 'reports-wsal').'</p></td>';
        $html .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('Start date', 'reports-wsal').'</p></td>';
        $html .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.__('End date', 'reports-wsal').'</p></td>';
        $html .= '</tr></thead>';
        $html .= '<tbody><tr>';
        $html .= '<td style="padding: 16px 7px;"><p style="margin: 0">List of unique IP addresses used by the same user</p></td>';
        $html .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v1.'</p></td>';
        $html .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$v2.'</p></td>';
        $html .= '</tr></tbody>';
        $html .= '</table>';
        $html .= '</table>';
        $html .= '</div>';
        fwrite($file, $html);
        fwrite($file, '<h4>'.__('Results', 'reports-wsal').':</h4>');
        fwrite($file, '<table class="wsal_report_table" style="border: solid 1px #333333;border-spacing:5px;border-collapse: collapse;margin: 0 0;width: 100%;font-size: 14px;">');
        $columns = array(
            'Username',
            'Display name',
            'Unique IP',
            'List of IP adresses'
        );
        $h = '';
        foreach ($columns as $item) {
            $h .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$item.'</p></td>';
        }
        fwrite($file, '<thead style="background-color: #555555;border: 1px solid #555555;color: #ffffff;padding: 0 0;text-align: left;vertical-align: top;"><tr>'.$h.'</tr></thead>');
        fwrite($file, '<tbody>');
        
        foreach ($data as $i => $element) {
            $r = ($i % 2 != 0) ? '<tr style="background-color: #f1f1f1;">' : '<tr style="background-color: #ffffff;">';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$element['user_login'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.$element['display_name'].'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.count($element['ips']).'</p></td>';
            $r .= '<td style="padding: 16px 7px;"><p style="margin: 0">'.'<ul><li>' . join('</li><li>', $element['ips']) . '</li></ul>'.'</p></td>';
            $r .= '</tr>';
            fwrite($file, $r);
        }
        fwrite($file, '</tbody></table>');
        fclose($file);
        return $fn;
    }
}
