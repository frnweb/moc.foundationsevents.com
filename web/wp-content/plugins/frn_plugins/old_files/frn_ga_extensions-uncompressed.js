//Some of this based on: http://jsfiddle.net/shamel67/VzbPw/1/
//coded by daxon edwards (daxon.me) 2013-2014
/*
//v2.0 - Added Hotjar event tracking capability
//v3.0 - Adapted JS to respond to new PHP triggers
		- Incorporated UA triggers into code 3/10/15 so it now works for new analytics approach and the old
		- Added better hotjar checking
		- Used JS validator
		- Should be using a JS compression tool as well
		- Added Pinterest social tracking
		- Added simpler social sharing tracker
		- Added social tracking for hotjar
		- Made email address clicks more specific to use email address in url\
		- fixed duplicated downloads tracking issue
//v3.1 - fixed external linking and target removing options
*/

//to do in future:
// Add LinkedIn social sharing tracking (for button--link is done)

//reducing errors
//since new PHP version, if variables present, then JS external link and target elements should be activated.
//if (typeof frn_eli_act === 'undefined') var frn_eli_act="";
if (typeof frn_eli_target === 'undefined') var frn_eli_target="";
if (typeof baseDomain === 'undefined') var baseDomain=document.location.hostname;  //also triggers ga logging
if (typeof frn_hj_act === 'undefined') var frn_hj_act="";



//#####
//### ENSURES GA INITIALIZED
//#####

	/**
	 * Ensure global _gaq or ga Google Analytics queue has been initialized.
	 */
	var _gaq = _gaq || [];
	if (typeof ga !== 'undefined') var _ga=true;
		else var _ga=false;
	
	/**
	 * Ensures global hj initialized
	 */
	if (frn_hj_act!=="") var hj = hj || {};
		else var hj = false;
	
	


//#####
//### SPECIAL SITE-WIDE EVENT TRACKING
//#####


///////////////////
// _trackOutbound
jQuery("body").ready(function($) {
//used to use "document" ready instead of "body"
	
	if ((_ga) || (_gaq) || frn_hj_act!=="") {
		
		//Email addresses
		$('a[href^="mailto"]').on('click', function(e) {
			if (_ga) ga('send', 'event', 'Email Address Clicks', this.href.replace(/^mailto:/i, ''));
				else if(_gaq) _gaq.push(['_trackEvent', 'Email Address Clicks', this.href.replace(/^mailto:/i, '')]);
				//alert("email reported");
			if (frn_hj_act!=="" && hj) {
				var email_clean = this.href.replace(/^mailto:/i, '');
				email_clean = email_clean.replace(/@\./i, ''); //strips out @ and periods to simplify url
				hj('vpv', '/link-clicked-to-email-'+email_clean+'/');
			}
		});
		
		
		//regular links
		var dom_pattern= new RegExp(location.host.replace('www.', ''),"i");
		
		//sees if the domain of the page is different than the domain in the href
		$('a[href^="http"]').filter(function() {
		   if ((dom_pattern).test($(this).attr('href'))) return false;
		   else return true;
		}).on('click', function(e) {
		
		//Old case sensitive version: $('a[href^="http"]:not([href*="//' + location.host + '"])').on('click', function(e) {
			//Sees if external link is a download and track if so
			var download_tracked = false;
			regex_pttn = /\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*)/i;
			if (regex_pttn.test(this.href)) { 
				download_tracked = true;
				if (_ga) ga('send', 'event', 'Images/Files', regex_pttn.exec(this.href)[1].toUpperCase(), "Off-Site Image/File: "+this.href.replace(/^.*\/\//, ''));
					else if (_gaq) _gaq.push(['_trackEvent', 'Images/Files', regex_pttn.exec(this.href)[1].toUpperCase(), "Off-Site Image/File: "+this.href.replace(/^.*\/\//, '')]);
				//alert("Outbound File Click Tracked: (" + regex_pttn.exec(this.href)[1].toUpperCase() + ") " + this.href.replace(/^.*\/\//, ''));
			}
			
			//Checks for non-iframe and non-DIV versions of sharing buttons/links
			var social_type = "";
			var social_action = "";
			if ($(this).attr('href').indexOf("pinterest.com/pin/create/button/") >=0 ) {
				social_type = "Pinterest";
				social_action = "pinned";
			}
			else if ($(this).attr('href').indexOf("linkedin.com/shareArticle") >=0 ) {
				social_type = "LinkedIn";
				social_action = "share";
			}
			else if ($(this).attr('href').indexOf("facebook.com/sharer/sharer.php") >=0 ) {
				social_type = "Facebook";
				social_action = "share";
			}
			else if ($(this).attr('href').indexOf("twitter.com/home?status") >=0 ) {
				social_type = "Twitter";
				social_action = "tweet";
			}
			
			//Tracks outbound link whether download or not.  Creates duplicate events if link is an offsite download, but this data will be tagged differently.
			if (_ga) { //new way
				//checks if href is a pinterest link or not, if not, then it gets standard reporting
				if (social_type!=="") {ga('send', 'social', social_type, social_action, this.href.match(/\/\/([^\/]+)/)[1], undefined); //alert("social link click sent to Google: "+social_type);
					}
				else { ga('send', 'event', 'Outbound Links', "Outbound: "+this.href.match(/\/\/([^\/]+)/)[1]); //alert("external link click sent to Google"); 
					}
				//alert("external link click sent to Google");
			}
			else if (_gaq) {  //old way
				//checks if href is a pinterest link or not, if not, then it gets standard reporting
				if (social_type!=="") _gaq.push(['_trackSocial', social_type, social_action, this.href.match(/\/\/([^\/]+)/)[1], undefined]);
				else _gaq.push(['_trackEvent', 'Outbound Links', "Outbound: "+this.href.match(/\/\/([^\/]+)/)[1]]);
			}
			
			//hotjar
			if (frn_hj_act!=="" && hj) {
				if (social_type!=="") hj('vpv', '/link-clicked-to-share-to'+social_type+'/');
				else hj('vpv', '/link-clicked-to-visit-external-link/');
			}
			social_type="";
		});
	

		///////////////////
		// _trackDownloads

		//If download has already been tracked since it's an external link, then skip this
		if(typeof download_tracked === 'undefined') {
			
			// helper function - allow regex as jQuery selector
			$.expr[':'].regex = function(e, i, m) {
				var mP = m[3].split(','),
					l = /^(data|css):/,
					a = {
						method: mP[0].match(l) ? mP[0].split(':')[0] : 'attr',
						property: mP.shift().replace(l, '')
					},
					r = new RegExp(mP.join('').replace(/^\s+|\s+$/g, ''), 'ig');
				return r.test($(e)[a.method](a.property));
			};

			$('a:regex(href,\\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*))').on('click', function(e) {
				//Make sure it's on this website
				var download_error = "";
				if (this.href.indexOf(baseDomain)>0) {
					regex_pttn = /\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*)/i;
					if (_ga) ga('send', 'event', 'Images/Files', regex_pttn.exec(this.href)[1].toUpperCase(), "On-Site Image/File: "+this.href.replace(/^.*\/\//, ''));
					else if (_gaq) _gaq.push(['_trackEvent', 'Images/Files', regex_pttn.exec(this.href)[1].toUpperCase(), "Off-Site Image/File: "+this.href.replace(/^.*\/\//, '')]);
					if (frn_hj_act!=="" && hj) hj('vpv', '/link-clicked-to-download-or-enlarge-image/');
					//alert("On-Site Download Tracked: (" + regex_pttn.exec(this.href)[1].toUpperCase() + ") " + this.href.replace(/^.*\/\//, ''));
				}
			});
		}


	}
	
	//The following is not tracked in hotjar, so the following only happens if any Google Analytics code is activated
	if ((_ga) || (_gaq)) {
	///////////////////
	// _trackError: track 404 - Page not found Errors

		if (typeof error_404_title === 'undefined') var error_404_title="Page Not Found";
		else if (error_404_title==="") error_404_title="Page Not Found";
		if (document.referrer==="") $site_referrer = "; Problem_on=[direct_visit/offsite]";
		else {
			if (document.referrer=="http://"+location.host) $site_referrer = "; Problem_on=" + document.referrer.replace("http://"+location.host+"/","[homepage]");
			else $site_referrer = "; Problem_on=" + document.referrer.replace("http://"+location.host,"");
		}
		if (document.title.search(error_404_title) >=0) {
			regex_pttn = /\.(zip|mp\\d+|mpe*g|pdf|docx*|pptx*|xlsx*|jpe*g|png|gif|tiff*)/i;
			if (regex_pttn.test(location.pathname)) {error_label = "On-Site Download (" + regex_pttn.exec(location.pathname)[1].toUpperCase() + ")"; error_type = "Download Error";}
			else {error_label = "Page Not Found"; error_type = "Page Not Found";}
			if (_ga) ga('send', 'event', '404 Errors', error_type + "; Broken_URL=" + location.pathname + $site_referrer, error_label);
			else if (_gaq) _gaq.push(['_trackEvent', '404 Errors', error_type + "; Broken_URL=" + location.pathname + $site_referrer, error_label]);
			//alert("404 tracked: " + error_404_title + "; " + document.title);
			//alert(error_type + "; Broken_URL=" + location.pathname + $site_referrer + "; Label:" + error_label);
		}
	}
	
	
	
	//Adds external link icon
	if (typeof frn_eli_deact !== 'undefined' ) { //both frn_eli_deact and frn_eli_target are defined at the same time if the JS version of these are selected in the plugin settings, so we only need to check one of them.
		var currStyle="", hasbgimg="", styleBg="", img_check="";
		if (typeof pluginInstall !== 'undefined') 
			var ext_icon = "background-image:url('"+pluginInstall+"/frn_plugins/images/icon_ext_links.png'); background-repeat:no-repeat; background-position:right center; padding-right:14px; margin-right: 5px;";
			else ext_icon="";
			
		//making frn_eli_deact=A should only happen if they specifically choose to use the JS version of this--which means there will never be a blank or undefined option by this point.
		if (frn_eli_target!="new" || (frn_eli_deact=="A")) {
			$('a[href^="http"]').filter(function() {
				   if ((dom_pattern).test($(this).attr('href'))) return false;
				   else return true;
				}).each(function(e) {
					//Old case sensitive version: $('a[href^="http"]:not([href*="//' + location.host + '"])').each(function(e) {
					//alert($(this).attr("href"));
					
					//making frn_eli_deact=A should only happen if they specifically choose to use the JS version of this--which means there will never be a blank or undefined option by this point.
					if (frn_eli_deact=="A" && !$(this).attr("data-exticon") && ext_icon!=="") { 
					//deactivates icon for external links if variable = A or if the link has data-exticon attribute defined
						//alert(currStyle);
						//check if inline styles present so we can just append to them
						currStyle=$(this).attr("style");
						if (typeof currStyle!=="undefined") { //if there is a style attribute manually added to the link, add a semicolon
							currStyle=currStyle.trim(); //removes end spaces
							if (currStyle.substr(currStyle.length - 1)!=";") currStyle=currStyle+"; "; //adds a semicolon if inline style doesn't have one
						}
						else currStyle="";
						
						//Check if image is linked or if css uses a background image for linking
						img_check = $(this).html();
						styleBg=$(this).css('background-image');
						if (img_check.indexOf("img")==-1 && styleBg=="none") 
							$(this).attr("style", currStyle+ext_icon);
					}
					//if there is a target set, check if we don't want to remove it.
					if((this).attr("target")!=='undefined') {
						//if the whole site setting is to open external links in the same window, check if we specifically wanted to open that one into a new window
						if (frn_eli_target=="same" && ($(this).attr("data-target")=="new" || $(this).attr("keep_target")=="yes")) ""; //do nothing and keep the target;
						else $(this).removeAttr( "target" );
					}
			});
		}
	
		//deactivates icon for email addresses if variable is not A
		//making frn_eli_deact=A should only happen if they specifically choose to use the JS version of this--which means there will never be a blank or undefined option by this point.
		if (frn_eli_deact=="A" && !$(this).attr("data-exticon" && ext_icon!=="")) { 
			$('a[href^="mailto"]').each(function(e) {
				currStyle=$(this).attr("style");
				if (typeof currStyle !== 'undefined') {
					currStyle=currStyle.trim(); //removes end spaces
					if (currStyle.substr(currStyle.length - 1)!=";") currStyle=currStyle+"; "; //adds a semi colon if inline style doesn't have one
				}
				else currStyle="";
				
				img_check = $(this).html();
				styleBg=$(this).css('background-image');
				if (img_check.indexOf("img")==-1 && styleBg=="none") 
					$(this).attr("style", currStyle+ext_icon);
			});
		}
	}
	
	
});






//#####
//### SOCIAL SHARING TRACKING
//#####

// authored by bravedick on GitHub, last updated Jan 26,2015
// modified by Daxon Edwards 5/11/2015
// https://github.com/bravedick/Google-Analytics-Social-Tracking

	/**
	 * https://developers.google.com/analytics/devguides/collection/analyticsjs/social-interactions
	 */

	var GASocialTracking = function() {

	};

	GASocialTracking.prototype = {
		/*
		 * Docs: https://dev.twitter.com/web/javascript/events
		 * */
		trackTwitter: function() {
			var _this = this,
				opt_target,
				network = 'twitter';

			try {
				// tweet
				twttr.events.bind('tweet', function(event) {
					if (event.target.nodeName.toUpperCase() == 'IFRAME') {
						opt_target = _this.getParamFromUrl(event.target.src, 'url');
					}
					_this.push(network, event.type, opt_target);
				});

				// follow
				twttr.events.bind('follow', function(event) {
					opt_target = event.data.user_id + ' (' + event.data.screen_name + ')';
					_this.push(network, event.type, opt_target);
				});

				// retweet
				twttr.events.bind('retweet', function(event) {
					opt_target = event.data.source_tweet_id;
					_this.push(network, event.type, opt_target);
				});

				// favorite
				twttr.events.bind('favorite', function(event) {
					opt_target = event.data.tweet_id;
					_this.push(network, event.type, opt_target);
				});
			} catch (e) {
				//if (console) console.log('GA Twitter Tracking error:', e);
			}
		},
		/*
		 * Docs: https://developers.facebook.com/docs/reference/javascript/FB.Event.subscribe/v2.2?locale=en_GB
		 * */
		trackFacebook: function() {
			var  _this = this,
				network = 'facebook';
			try {
				FB.Event.subscribe('edge.create', function(url) {
					_this.push(network, 'like', url);
				});
				FB.Event.subscribe('edge.remove', function(url) {
					_this.push(network, 'unlike', url);
				});
				FB.Event.subscribe('message.send', function(url) {
					_this.push(network, 'message', url);
				});
				FB.Event.subscribe('comment.create', function(obj) {
					_this.push(network, 'comment.create', obj.href);
				});
				FB.Event.subscribe('comment.remove', function(obj) {
					_this.push(network, 'comment.remove', obj.href);
				});
			} catch (e) {
				//if (console) console.log('GA Facebook Tracking error:', e);
			}
		},
		push: function(network, socialAction, opt_target) {
			if (_ga) ga('send', 'social', network, socialAction, opt_target, undefined);
			else if (_gaq) _gaq.push(['_trackSocial', network, socialAction, opt_target, undefined]); //old GA version
			if (frn_hj_act!=="" && hj) hj('vpv', '/link-clicked-to-share-to'+network+'/');
		},
		getParamFromUrl: function(url, param) {
			var pairs = url.split('&');
			for (var i = 0; i < pairs.length; i++) {
				var pair = pairs[i].split('=');
				if (pair[0] == param) {
					return decodeURIComponent(pair[1]);
				}
			}
			return false;
		}
	};

	var gaSocialTracking = new GASocialTracking();

