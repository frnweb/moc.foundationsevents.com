<?php

// This PHP file adds the Related Posts by Keyword options SUB menu page to the main FRN Settings Menu
add_action('admin_menu', 'frn_plugin_subpage_rps');
function frn_plugin_subpage_rps() {
	add_submenu_page( 'frn_features',"FRN Automated Related Posts Options", "Related Posts", 'manage_options', 'frn_related_section', 'frn_subpage_rps_layout');
	add_settings_section('site_rp', '', '', 'frn_related_section');
		add_settings_field('rp_fields', "Implementation Options <a href='javascript:showhide(\"frn_plugin_related_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a>", 'frn_related_posts_sc', 'frn_related_section', 'site_rp');	
		add_settings_field('frn_related_posts_auto', "Automatic Related Posts <a href='javascript:showhide(\"frn_plugin_rpauto_hlp\")' ><img src='".$GLOBALS['help_image']."' /></a>", 'frn_related_posts_auto', 'frn_related_section', 'site_rp');	
	add_action('admin_init', 'frn_rps_variable');
}

function frn_rps_variable() {
	//registers the variable for the database
	register_setting( 'frn_plugin_rp', 'site_rp','plugin_options_auto_rp_save'  ); 
}



function frn_subpage_rps_layout() {
	?>
	<div class="wrap">
		<h2>FRN Plugin: Automated Related Posts</h2>
		<div class="intro_text" style="margin-top:50px;">
			<p>By default, this features takes the words in the current page's or post's URI and searches the site for other posts that may relate. You can manually add them to a page using the shortcode or turn on the automated option and choose your posts types. You can use both as well. Wherever you put the shortcode, that's where the list will show.</p>
			<p>It comes with a predetermined styles just in case that works for the site to save you time. But you can disable those styles and use your own CSS instead. There are numerous customization options. This same shortcode is used on many facility 404 pages and some niche sites.</p>
			<p>Click the help icons <img src="<?=$GLOBALS['help_image'];?>" /> to know the options for each section.</p>
			<br />
			<hr />
			<br />
		</div>
		<form action="options.php" method="post" class="frn_styles">
			<?php
				settings_fields( 'frn_plugin_rp' );
				do_settings_sections( 'frn_related_section' );
			?>
			<table class="form-table">
				<tr>
					<th></th>
					<td>
						<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
					</td>
				</tr>
			</table>
			</div>
		</form>
	</div>
	<?php 
}




//////////
// RELATED_POSTS by Keyword (Shortcode)
//////////
function frn_related_posts_sc($input) {	
	
	?>
	This function produces a list of posts based on a keyword search. Keywords are pulled from the page's URL, but can be manually provided. Install <a href="https://wordpress.org/plugins/relevanssi/" target="_blank">Relevanssi</a> for best results. View <a href='javascript:showhide("frn_plugin_related_hlp")' >help</a> for options/tips.
	<div class="frn_options_table"><table class="frn_options_table"><tr>
			<td valign="top">Shortcode for Text: </td>
			<td valign="top"><b><span id="frn_related_posts_sc" class="frn_shortcode_sel" onClick="selectText('frn_related_posts_sc')">[frn_related html="" total="" search="" post_type="" no_results_msg=""]</span></b> <font size="1">(attributes optional)</font></td>
		</tr><tr>
			<td valign="top">Shortcode for PHP: </td>
			<td valign="top"><b><span id="frn_related_posts_php" class="frn_shortcode_sel" onClick="selectText('frn_related_posts_php')">&lt;?php echo do_shortcode('[frn_related]'); ?&gt;</span></b></td>
	</tr><tr>
			<td valign="top">Function: </td>
			<td valign="top"><b><span id="frn_related_posts_funct" class="frn_shortcode_sel" onClick="selectText('frn_related_posts_funct')">&lt;?php if(function_exists('frn_related_list')) { $related_posts_array=frn_related_list("array", 8, ""); } ?&gt;</span></b> <font size="1">(see ? for details)</font></td>
	</tr></table></div>
	<div id='frn_plugin_related_hlp' class='frn_help_boxes'>
	<ul class="frn_level_1">
		<li>This function is exactly the same as going into the theme's search field and doing a search. Whatever your settings are for the WP search, this function will return the same results.</li>
		<li style="margin-top:15px;"><b>SEARCH="": </b>
			<ul class="frn_level_2">
				<li>By default, it uses the URL to get it's keywords unless you put something in the SEARCH variable.</li>
				<li>Including the SEARCH attribute will override the default URL search and instead search on whatever words you put there. This lets you use the shortcode to return any search you'd like--inpreperation for a better related posts.
				<li>When relying on a URL, like in 404 pages, all punctuation, dashes, underscores, stop words, and numbers will be removed. The only thing removed when you provide your own terms is stop words like a, an, the, is, etc.</li>
			</ul>
		</li>
		<li style="margin-top:15px;"><b>HTML Options:</b>
			<ul class="frn_level_2">
				<li><b>HTML="":</b> (DEFAULT) Not including the HTML variable or leaving it blank will just return an HTML list of related posts using &lt;ul&gt; and &lt;li&gt;.</li>
				<li><b>HTML="404":</b> If you use HTML="404", it'll print out our typical content for drug addiction related sites: 404 header, intro message, no results message, a search box with the list, and final message to call us for help.</li>
				<li><b>HTML="searchbox":</b> Use this if you want a typical HTML list followed by the site's search box. It will pull in the typical searchbox using WP's "get_search_form(false)" function, which uses what's in search.php. It's automatically wrapped in a class "frn_search_box" to help you style the 404 box specifically.</li>
				<li><b>HTML="array":</b> Similar to using just the function, you can use the shortcode to return an array that you set to a variable. You can then loop through results. It was easy, so I just threw it in. It'll return false if nothing is found.</li>
			</ul>
		</li>
		<li><b>post_type="":</b> By default, this searches all published post types in the site but media. If you want more than one post type, but not all of them, include a comma seperated list of the ones you want (e.g. "post,page,stories"). This needs to be the exact post types defined in WordPress. If you want to search custom post types, go to the list of posts in the admin. "post_type" will be at the end of the URL.</li>
		<li><b>no_results_msg="":</b> When search returns nothing for the 404 or default list, you can customize what is returned by adding this attribute. When HTML is not used, use "blank" if you want nothing returned when nothing is found.</li>
		<li><b>FUNCTION DIRECTLY:</b> Just like the shortcode, it takes four items. If you use "array" as the first one, it'll only return an array for you to loop through. If there are no results, the function returns "FALSE" (i.e. fails) so you can test it before printing. The second position is how many you want to return and the third spot is for search terms in case you don't want to use terms in the url.</li>
		<li><b>TOTAL="":</b> This is used to change the total number of items you want returned. By default, it's 8.</li>
		<li style="margin-top:15px;"><b>For Better Results:</b> 
			<ul class="frn_level_2">
				<li>If the <a href="https://wordpress.org/plugins/relevanssi/" target="_blank">Relevanssi</a> plugin is installed, the results are exactly according to those settings. It'll show the most relevant ones.</li>
				<li><b>TROUBLESHOOTING:</b> If you keep getting no results, try the Relevanssi plugin. WP's search requires a post to have all the words you search on or it returns nothing. Especially in 404 situations, it's pretty useless to the user.</li>
			</ul>
		</li>
	</ul>
	</div>
	<?php
}
function frn_related_posts_auto() {
	//This function is used as a content filter that adds a related posts section at the bottom of content
	//The powerful piece of this is that you can help it recognize the DIV ID for the section of resources for posts so that RPs can be added above that section.

	//activation
	//post types
	//include
	//exclude
	//default search term
	//quantity
	//ID of sources DIV
	//style: list, tiled, image & text

	$rp=get_option('site_rp');
	if(!isset($rp['activation'])) $rp['activation']="";


	////////
	// Activation
	?>
	<input type="checkbox" name="site_rp[activation]" value="yes" <?=($rp['activation']=="yes") ? "checked" : null;?> onClick="showhide('frn_auto_rp_wrap')" /> Activate
	<div id="frn_plugin_rpauto_hlp" class="frn_help_boxes" style="display:none;">
		<b>Activation Notes</b>
		<ul class="frn_level_1">
			<li><b>Why Should You Use This? </b>
				<ol class="frn_level_2">
					<li>Usability: We have a challenge keeping people on our site. Often, they get to the bottom of posts and want to learn more but it's too hard to figure how. Context boxes help with this in the middle of content, but related posts can help with at the end.</li>
					<li>Rehab Decision Journey: This tool will eventually expand into our rehab journey, but for now we can get closer to suggesting posts on new topics instead of posts on the same topic.</li>
					<li>SEO: Interlinking is a powerful way to point Google to more important content and better relate topics semantically. We are typically weak on the front, so this will automatically improve that for us.</li>
				</ol>
			</li>
			<li><b>Content Hook:</b> This feature "hooks" into the content feature of WordPress. There isn't a set order, but any plugin or theme can hook into this. It's possible one of those may interfere with this.</li>
			<li><b>Automatically Excluded: </b>By default, the following page types and situations will not use this feature.
				<ul class="frn_level_2">
					<li>[frn_related] -- When shortcode is already in content</li>
					<li>Front Page</li>
					<li>Home Page</li>
					<li>Listing pages (e.g. blog homepage, tags, and categories)</li>
					<li>Contact Page (requires "contact" to be in the title)</li>
					<li>Attachment pages</li>
					<li>Revisions</li>
					<li>Nav_menu_item page types</li>
				</ul>
			</li>
			<li><b>Troubleshooting:</b>
			<ul>
				<li></li>
				<li></li>
			</ul>
			</li>
		</ul>
	</div>
	<div id="frn_auto_rp_wrap" style="display:<?=($rp['activation']=="yes") ? 'block' : 'none' ;?>">
		<?php





		////////
		// Activate on These Post Types

		if(!isset($rp['post_types'])) $rp['post_types']="";
		if(!isset($rp['search_types'])) $rp['search_types']="";
		?>

		<h3 style="font-size: 15px;">Select Post Types to Add RPs To: <a href="javascript:showhide('frn_plugin_rp_hlp')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
		<?php //print_r($rp['post_types']);?>
		<div>
		<?php
			$args=array(
				'public'   => true,
				);
			$post_types = get_post_types( $args, 'names' );
			//print_r($rp['post_types']);
			if($post_types) {
				foreach($post_types as $post_type) {
					if($post_type!=="attachment" && $post_type!=="revision" && $post_type!=="nav_menu_item") { 
						$selected="";
						if(is_array($rp['post_types'])) {
							foreach($rp['post_types'] as $selected_type) {
								if($selected=="") {if($post_type==$selected_type) $selected="checked";}
							}
						}
		?>
			<div style="float:left; width:33%; min-width:100px;" ><input type="checkbox" name="site_rp[post_types][]" value="<?=$post_type; ?>" <?=$selected;?> /> <?=$post_type; ?></div>
		<?php
					}
				}
			}
			else echo "<p>No \"Public\" Post Types Found</p>";
		?>
		</div>
		<div id="frn_plugin_rp_hlp" class="frn_help_boxes" style="display:none; float: left; width: 100%;">
			<b>Post Types Help</b>
			<ul class="frn_level_1">
				<li>Related posts will not show unless specific post types are selected.</li>
				<li>This lists all the "public" post types on the site.</li>
				<li>If a type isn't above an it should be, make sure it's set to be public when registering the post type in the functions.php file (or wherever the function is).</li>
				<li>Select all the post types you want related posts automatically added below content.</li>
				<li>Only when you select a post type will the "exclude" IDs be activated.</li>
				<li>The "include" IDs is activated whether or not you select a post type. Of course, if the included post ID's type is also the same as the selected post type, then it's redundant.</li>
			</ul>
		</div>


		<h3 style="font-size: 15px; float: left; width: 100%;">Limit Recommended Articles by Post Type: </h3>
		
		<div>
			<?php
			if($post_types) {
				$sel_count=0;
				foreach($post_types as $post_type) {
					if($post_type!=="attachment" && $post_type!=="revision" && $post_type!=="nav_menu_item") { 
						$selected="";
						if(is_array($rp['search_types'])) {
							foreach($rp['search_types'] as $selected_type) {
								if($selected=="") {
									if($post_type==$selected_type) {$selected="checked"; $sel_count++;}
								}
							}
						}
			?>
			<div style="float:left; width:33%; min-width:100px;" ><input type="checkbox" name="site_rp[search_types][]" value="<?=$post_type; ?>" <?=$selected;?> /> <?=$post_type; ?></div>
			<?php
					}
				}
				///////
				/// ANY option consideration
				//"Any" is NOT a post type, but it is a WP_query option. 
				//But we don't want "any" showing as a post type when doing a wp_query. The frontend of this feature removes "any" from an array when building the wp_query args
				//The wp_query default type searched is post, so having nothing checked is okay.
				//Since "any" is not a typical post type, we need to manually add it as an option
				//this is outside the loop since it would be duplicated if added to the search_types check above
				$any_select="";
				if(is_array($rp['search_types'])) {
					if(in_array("any",$rp['search_types'])) { $any_select="checked"; }
				}
			?>
			<div style="float:left; width:33%; min-width:100px;" ><input type="checkbox" name="site_rp[search_types][]" value="any" <?=$any_select;?> /> [All Types]</div>
			<?php
			} //ends requirement that wp post_types are returned

			//The following will likely never happen unless there is an miracle error
			else echo "<p>No \"Public\" Post Types Found</p>";
			?>
		</div>


		<?php




		////////
		// Specific Pages

		if(!isset($rp['include'])) $rp['include']="";
		if(!isset($rp['exclude'])) $rp['exclude']="";
		?>
		<h3 style="font-size: 15px; float: left; width: 100%;">Specific Page Limitations: <a href="javascript:showhide('frn_plugin_rp_hlp_sp')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
		<table class="frn_options_table">
			<tr>
				<td>Use Only In: </td><td><input id="frn_rp_incl" name="site_rp[include]"" size="30" type="text" value="<?=$rp['include'];?>" /></td>
			</tr>
			<tr>
				<td>DO NOT Use In: </td><td><input id="frn_rp_excl" name="site_rp[exclude]"" size="30" type="text" value="<?=$rp['exclude'];?>" /></td>
			</tr>
		</table>
		<div id="frn_plugin_rp_hlp_sp" class="frn_help_boxes" style="display:none;">
			<b>Specific Pages Help</b>
			<ul class="frn_level_1">
				<li><b>Use Only In: </b>
				<ul class="frn_level_2">
					<li>Use a comma seperated list of post IDs where you specifically want related posts to show. </li>
					<li>Including even one ID will override any post type selection above.</li>
				</ul></li>
				<li><b>DO NOT Use In: </b>
				<ul class="frn_level_2">
					<li>Use a comma seperated list of post IDs where you specifically don't want related pages to show. </li>
					<li>This is only activated if you have selected post types above.</li>
				</ul></li>
			</ul>
		</div>

		<?php




		////////
		// Advanced Options

		if(!isset($rp['term'])) $rp['term']="";
		if(!isset($rp['count'])) $rp['count']=3;
			elseif($rp['count']=="") $rp['count']=3;
		if(!isset($rp['refs_id'])) $rp['refs_id']="";
		
		// Disable WP Cache
			
			$checked=""; $style_checked="";
			if(!isset($rp['cache'])) $rp['cache']=""; 
			if($rp['cache']!=="") $checked=" checked";

			if(!isset($rp['default_styles'])) $rp['default_styles']="Yes"; //for first time use, this is checked. A person will have to manually uncheck this to use their own styles.
			if($rp['default_styles']!=="") $style_checked=" checked";
		
		//if(!isset($rp['layout'])) $rp['layout']="";
		//if(!isset($rp['style'])) $rp['style']="";
		
		?>
		<h3 style="font-size: 15px; float: left; width: 100%;">Advanced Options (<a href="javascript:showhide('frn_options_rp_adv')" >show</a>) <a href="javascript:showhide('frn_plugin_rp_hlp_style')" ><img src="<?=$GLOBALS['help_image'];?>" /></a></h3>
		<table id="frn_options_rp_adv" class="frn_options_table" style="display:none;" >
			<tr>
				<td>Use Default Styles:</td>
				<td><input type="checkbox" name="site_rp[default_styles]" value="Y" <?=$style_checked;?> /></td>
			</tr>
			<tr>
				<td>Overwrite Section Title: </td><td><input name="site_rp[h2]"" size="30" type="text" value="<?=$rp['h2'];?>" /></td>
			</tr>
			<tr>
				<td>Default Search Term: </td><td><input id="frn_rp_term" name="site_rp[term]"" size="30" type="text" value="<?=$rp['term'];?>" /></td>
			</tr>
			<tr>
				<td>Total in List: </td><td><input id="frn_rp_count" name="site_rp[count]"" size="10" type="text" value="<?=$rp['count'];?>" /></td>
			</tr>
			<tr>
				<td>Overwrite DIV CLASS: </td><td><input id="frn_rp_class" name="site_rp[class]"" size="30" type="text" value="<?=$rp['class'];?>" /></td>
			</tr>
			<tr>
				<td>Overwrite DIV ID: </td><td><input id="frn_rp_id" name="site_rp[id]"" size="30" type="text" value="<?=$rp['id'];?>" /></td>
			</tr>
			<?php /* //The following are future features.
			<tr>
				<td>Layout: </td>
				<td><select id="frn_rp_layout" name="site_rp[layout]">
						<option value="list" <?=($rp['layout']=="list" || $rp['layout']=="") ? "selected" : null ;?>>Bulleted List (Default)</option>
						<option value="tiles" <?=($rp['layout']=="tiles") ? "selected" : null ;?>>Tiles</option>
						<option value="imagetext" <?=($rp['layout']=="imagetext") ? "selected" : null ;?>>Images & Text</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Use Default Styles: </td><td><input id="frn_rp_style" name="site_rp[style]"" size="30" type="text" value="<?=$rp['style'];?>" /></td>
			</tr>
			<tr>
				<td>Article Sources ID: </td><td><input id="frn_rp_id" name="site_rp[citations_id]"" size="30" type="text" value="<?=$rp['citations_id'];?>" /></td>
			</tr>
			*/ ?>
			<tr>
				<td>Disable Cache:</td>
				<td><input type="checkbox" name="site_rp[cache]" value="Y" <?=$checked;?> /></td>
			</tr>
		</table>
		

		<div id="frn_plugin_rp_hlp_style" class="frn_help_boxes" style="display:none;">
			<b>Content Options Help</b>
			<ul class="frn_level_1">
				<li><b>Default Search Term: </b>By default, the tool uses keywords from the URL. But if you'd like all pages to use the same search, you can specify that here.</li>
				<li><b>Overwrite Section Title: </b>By default, the header of the related posts is "You May Also Like". But you can overwrite that if you'd like. If you overwrite that with the same title regularly, suggest the change be network wide and added to this plugin as a default.</li>
				<li><b>Total in List: </b> The default is five. But depending on the design and space, you may want more or less. Enter the maximum number of posts you want displayed in this section.</li>
				<li><b>Overwrite Class: </b>By default, the list of related posts are wrapped by a DIV using the class "frn_url_results". You can overwrite that with your own class if you'd like.</li>
				<li><b>Overwrite DIV ID: </b>The TOC feature uses this to identify and skip providing an anchor to this section. To overwrite it, simply type the ID you prefer into the field and it'll be added just like you type it here. Use an ID to do more dynamic things with JavaScript or similar interactions.</li>
				<li><b>Limit Results by Post Type: </b>By default, the site will search all public pages, posts, and custom post types. But if you want to restrict results to just a few post types, place a checkbox in front of each.</li>
				<!--<li><b>Article Sources ID: </b>"Sources" means the source citations at the bottom of articles. Adding a box for related posts under sources looks akward and more likely to be ignored by readers. This system looks for sources in content and places related posts above it. It's a bit more resource heavy and it only specifically looks for the word "sources" or "citations" or "references" and places related posts immediately above the word. That won't work for all situations. Wrapping sources on all pages with a DIV and ID, allows you to customize the position on many pages easier. You can also use the shortcode to position easily. It's unlikely this feature will ever be used, but it's included in case we design a site with a custom sources field.</li>-->
				<li><b>Caching: </b>Using a search every time a page is loaded is pretty resource heavy. Caching reduces how often that happens. By default, the cache refreshes for each page every <b>four hours</b>. It is not activated on 404 pages. If you added a new post and you want it to show as a related post, disable the cache. The cache will only refresh when a page is loaded after the four hour timeframe. If you'd like to know more, read about <a href="https://codex.wordpress.org/Transients_API" target="_blank">WordPress's Transient feature</a>.</li>
			</ul>
		</div>



	</div><?php //end wrap to hide features when in shortcode only mode?>
	<?php 
}
function plugin_options_auto_rp_save($input) {
	//trim spacing from use inputed values
	$input['include']	=	trim($input['include']);
	$input['exclude']	=	trim($input['exclude']);
	$input['term']		=	trim($input['term']);
	$input['count']		=	trim($input['count']);
	if($input['count']==5) $input['count']=""; //removing default to allow system wide changing of this if needed
	$input['citations_id']	=	trim($input['citations_id']);
	$input['class']		=	trim($input['class']);
	$input['id']		=	trim($input['id']);

	return $input;
}


