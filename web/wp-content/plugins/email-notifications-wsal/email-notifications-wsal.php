<?php
/*
Plugin Name: Email Notifications for WP Security Audit Log
Plugin URI: http://www.wpwhitesecurity.com/wordpress-security-plugins/wp-security-audit-log/
Description: Configure notification triggers to be alerted via email of important changes on your WordPress website.
Author: WP White Security
Version: 2.2.0
Text Domain: email-notifications-wsal
Author URI: http://www.wpwhitesecurity.com/
License: GPL2

    WP Security Audit Log
    Copyright(c) 2014  Robert Abela  (email : robert@wpwhitesecurity.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * Holds the option prefix
 */
define('WSAL_OPT_PREFIX', 'notification-');
/**
 * Holds the maximum number of notifications a user is allowed to add
 */
define('WSAL_MAX_NOTIFICATIONS', 50);
/*
 * Holds the name of the cache key if cache available
 */
define('WSAL_CACHE_KEY', '__NOTIF_CACHE__');
/*
 * Debugging true|false
 */
define('WSAL_DEBUG_NOTIFICATIONS', false);

class WSAL_NP_Plugin
{
    protected $wsal = null;
    // Cache
    private $_notifications = null;
    private $_cacheExpire = 43200; // 12h (60*60*12)

    public function __construct()
    {
        add_action('admin_notices', array($this, 'admin_notice'));
        add_action('network_admin_notices', array($this, 'admin_notice'));
        add_action('wsal_init', array($this, 'wsal_init'));
        // Listen for activation event
        register_activation_hook(__FILE__, array($this, 'wizard_plugin_activate'));
        add_action('admin_init', array($this, 'wizard_plugin_redirect'));
        add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'add_action_links'));

        add_action('wp_login_failed', array($this, 'counterLoginFailure'));
        add_filter('template_redirect', array($this, 'counterEvent404'));
    }

    public function admin_notice()
    {
        if (is_main_site()) {
            if (!class_exists('WpSecurityAuditLog') && !defined('WSAL_REQUIRED_WARNING')) {
                define('WSAL_REQUIRED_WARNING', true);
                ?><div class="error"><p><?php _e('The <strong>WP Security Audit Log</strong> plugin must be installed and enabled for the <strong>Email Notifications Add On</strong> to function.', 'email-notifications-wsal'); ?></p></div><?php
            }
        }
    }

    public function wsal_init(WpSecurityAuditLog $wsal)
    {
        $wsal->licensing->AddPremiumPlugin(__FILE__);
        $wsal->autoloader->Register('WSAL_NP_', dirname(__FILE__) . '/classes');
        $wsalCommon = new WSAL_NP_Common($wsal);
        $wsal->wsalCommon = $wsalCommon;
        $wsal->views->AddFromClass('WSAL_NP_Notifications');
        $c = new WSAL_NP_Wizard($wsal);

        if (isset($_REQUEST['page'])) {
            $a = new WSAL_NP_AddNotification($wsal);
            $b = new WSAL_NP_EditNotification($wsal);
            $addNotifPageName = $a->GetSafeViewName();
            $editNotifPageName = $b->GetSafeViewName();
            $wizardfPageName = $c->GetSafeViewName();

            switch ($_REQUEST['page']) {
                case $addNotifPageName:
                    $wsal->views->AddFromClass('WSAL_NP_AddNotification');
                    break;
                case $editNotifPageName:
                    $wsal->views->AddFromClass('WSAL_NP_EditNotification');
                    break;
                case $wizardfPageName:
                    $wsal->views->AddFromClass('WSAL_NP_Wizard');
                    break;
            }
        }

        $wsal->alerts->AddFromClass('WSAL_NP_Notifier');
        $this->wsal = $wsal;
    }

    public function wizard_plugin_activate()
    {
        add_option('wizard_plugin_do_activation_redirect', true);
    }

    public function wizard_plugin_redirect()
    {
        if (get_option('wizard_plugin_do_activation_redirect', false)) {
            delete_option('wizard_plugin_do_activation_redirect');
            $wsal = WpSecurityAuditLog::GetInstance();
            $wsal->licensing->AddPremiumPlugin(__FILE__);
            $wsal->autoloader->Register('WSAL_NP_', dirname(__FILE__) . '/classes');
            $wsal->views->AddFromClass('WSAL_NP_Wizard');
            $wizard = new WSAL_NP_Wizard($wsal);
            //wp_enqueue_script('jquery.modal-js', $pluginPath.'/js/jquery.modal/jquery.modal.js', array('jquery'));
            //wp_enqueue_style('jquery.modal-css', $pluginPath.'/js/jquery.modal/jquery.modal.css');
            ?>
            <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" />
            <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
            <style type="text/css">
            .modal-content {
                border-radius: 0;
            }
            .modal-footer {
                padding-top: 0;
                border-top: none;
            }
            .btn {
                border-radius: 0px;
            }
            </style>
            <div class="modal fade" id="msg_modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Email Notifications for WP Security Audit Log</h4>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to launch the wizard to configure your first email notification alerts? If you select no you can launch it later or configure the email alerts manually.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo esc_attr($wizard->GetUrl() . '&first-time=1'); ?>" class="btn btn-primary">Launch Wizard</a>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No thank you</button>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                jQuery('#msg_modal').modal('show');
            </script>  
            <?php
            //exit(wp_redirect($wizard->GetUrl() . '&first-time=1'));
        }
    }

    public function add_action_links($links)
    {
        $new_links = array(
            '<a href="' . admin_url('admin.php?page=wsal-np-notifications') . '">Configure Email Alerts</a>'
        );
        return array_merge($new_links, $links);
    }

    /**
     * Triggered by Failed Login Hook
     * to increase the limit changes the max value when you call: $Notifications->CreateSelect()
     */
    public function counterLoginFailure($username)
    {
        $alertCode = 1003;
        $username = array_key_exists('log', $_POST) ? $_POST["log"] : $username;
        $user = get_user_by('login', $username);
        if ($user) {
            $alertCode = 1002;
        }
        if (!$this->wsal->alerts->IsEnabled($alertCode)) {
            return;
        }
        $site_id = (function_exists('get_current_blog_id') ? get_current_blog_id() : 0);
        $ip = $this->wsal->settings->GetMainClientIP();

        $this->_notifications = wp_cache_get(WSAL_CACHE_KEY);

        if (false === $this->_notifications) {
            $this->_notifications = $this->wsal->wsalCommon->GetNotifications();
            wp_cache_set(WSAL_CACHE_KEY, $this->_notifications, null, $this->_cacheExpire);
        }
        if (!empty($this->_notifications)) {
            foreach ($this->_notifications as $k => $v) {
                $notInfo = unserialize($v->option_value);
                $enabled = intval($notInfo->status);
                if ($enabled == 0) {
                    continue;
                }
                if (!empty($notInfo->failUser) && $user) {
                    if ($this->wsal->wsalCommon->IsLoginFailureLimit($notInfo->failUser, $ip, $site_id, $user, true)) {
                        break;
                    }
                    $this->wsal->wsalCommon->CounterLoginFailure($ip, $site_id, $user);

                    if ($this->wsal->wsalCommon->IsLoginFailureLimit($notInfo->failUser, $ip, $site_id, $user)) {
                        $this->sendSuspiciousActivity($notInfo, $ip, $site_id, $alertCode, $username);
                    }
                }
                if (!empty($notInfo->failNotUser)) {
                    if ($this->wsal->wsalCommon->IsLoginFailureLimit($notInfo->failNotUser, $ip, $site_id, null, true)) {
                        break;
                    }
                    $this->wsal->wsalCommon->CounterLoginFailure($ip, $site_id, $user);

                    if ($this->wsal->wsalCommon->IsLoginFailureLimit($notInfo->failNotUser, $ip, $site_id, null)) {
                        $this->sendSuspiciousActivity($notInfo, $ip, $site_id, $alertCode, $username);
                    }
                }
            }
        }
    }

    /**
     * Triggered by 404 Redirect Hook
     * to increase the limit changes the max value when you call: $Notifications->CreateSelect()
     */
    public function counterEvent404()
    {
        if (!$this->wsal->alerts->IsEnabled(6007)) {
            return;
        }
        global $wp_query;
        if (!$wp_query->is_404) {
            return;
        }

        if (!is_user_logged_in()) {
            $username = "Website Visitor";
        } else {
            $username = wp_get_current_user()->user_login;
        }
        $site_id = (function_exists('get_current_blog_id') ? get_current_blog_id() : 0);
        $ip = $this->wsal->settings->GetMainClientIP();

        $this->_notifications = wp_cache_get(WSAL_CACHE_KEY);

        if (false === $this->_notifications) {
            $this->_notifications = $this->wsal->wsalCommon->GetNotifications();
            wp_cache_set(WSAL_CACHE_KEY, $this->_notifications, null, $this->_cacheExpire);
        }
        if (!empty($this->_notifications)) {
            foreach ($this->_notifications as $k => $v) {
                $notInfo = unserialize($v->option_value);
                $enabled = intval($notInfo->status);
                if ($enabled == 0) {
                    continue;
                }
                if (!empty($notInfo->error404)) {
                    if ($this->wsal->wsalCommon->Is404Limit($notInfo->error404, $site_id, $username, $ip, true)) {
                        break;
                    }
                    $this->wsal->wsalCommon->Counter404($site_id, $username, $ip);

                    if ($this->wsal->wsalCommon->Is404Limit($notInfo->error404, $site_id, $username, $ip)) {
                        $this->sendSuspiciousActivity($notInfo, $ip, $site_id, 6007, $username);
                    }
                }
            }
        }
    }

    private function sendSuspiciousActivity($notInfo, $ip, $site_id, $alertCode, $username)
    {
        $title = $notInfo->title;
        $emailAddress = $notInfo->email;

        $alert = $this->wsal->alerts->GetAlert($alertCode);
        $user = get_user_by('login', $username);
        $userRole = '';
        if (!empty($user)) {
            $user_info = get_userdata($user->ID);
            $userRole = implode(', ', $user_info->roles);
        }
        $date = $this->wsal->wsalCommon->GetEmailDatetime();
        $blogname = $this->wsal->wsalCommon->GetBlogname();

        $count = 1;
        $search = array('%Attempts%', '%Msg%', '%LinkFile%');
        if (!empty($notInfo->failUser)) {
            $replace = array($notInfo->failUser, '', '');
        } else if (!empty($notInfo->failNotUser)) {
            $replace = array($notInfo->failNotUser, '', '');
        } else if (!empty($notInfo->error404)) {
            $replace = array($notInfo->error404, 'times', '');
        }
        $message = str_replace($search, $replace, $alert->mesg);

        $search = array('{title}', '{source_ip}', '{alert_id}', '{date_time}', '{message}', '{username}', '{user_role}', '{site}');
        $replace = array($title, $ip, $alertCode, $date, $message, $username, $userRole, $blogname);

        $template = $this->wsal->wsalCommon->GetEmailTemplate("built-in");
        $subject = str_replace($search, $replace, $template['subject']);
        $content = str_replace($search, $replace, stripslashes($template['body']));

        $result = $this->wsal->wsalCommon->SendNotificationEmail($emailAddress, $subject, $content, $alertCode);
    }
}
return new WSAL_NP_Plugin();
