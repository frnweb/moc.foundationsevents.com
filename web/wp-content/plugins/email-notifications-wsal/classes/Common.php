<?php if(! defined('WSAL_OPT_PREFIX')) { exit('Invalid request'); }
/**
 * Class WSAL_NP_Common
 *
 * Utility class
 */
class WSAL_NP_Common
{
    public $wsal = null;

    const TRANSIENT_FAILED_COUNT = 'wsal-notifications-failed-known-count';
    const TRANSIENT_FAILED_UNKNOWN_COUNT = 'wsal-notifications-failed-unknown-count';
    const TRANSIENT_404_COUNT = 'wsal-notifications-404-count';

    public function __construct(WpSecurityAuditLog $wsal)
    {
        $this->wsal = $wsal;
    }

    /**
     * Creates an unique random number
     * @param int $size The length of the number to generate
     * @return string
     */
    public function UniqueNumber($size = 20) {
        $numbers = range(0, 100);
        shuffle($numbers);
        $n = join('', array_slice($numbers, 0, $size));
        return substr($n, 0, $size);
    }

    public function AddGlobalOption($option, $value){
        $this->DeleteCacheNotif();
        $this->wsal->SetGlobalOption($option, $value);
    }

    public function UpdateGlobalOption($option, $value){
        $this->DeleteCacheNotif();
        return $this->wsal->UpdateGlobalOption($option, $value);
    }

    public function DeleteGlobalOption($option){
        $this->DeleteCacheNotif();
        return $this->wsal->DeleteByName($option);
    }

    public function GetOptionByName($option){
        return $this->wsal->GetGlobalOption($option);
    }

    public function DeleteCacheNotif(){
        if(function_exists('wp_cache_delete')){
            wp_cache_delete(WSAL_CACHE_KEY);
        }
    }

    /**
     * Retrieve the appropriate posts table name
     * @param $wpdb
     * @return string
     */
    public function GetPostsTableName($wpdb){
        $pfx = $this->GetDbPrefix($wpdb);
        if($this->wsal->IsMultisite()){
            global $blog_id;
            $bid = ($blog_id==1 ? '' : $blog_id.'_');
            return $pfx.$bid.'posts';
        }
        return $pfx.'posts';
    }

    /**
     * Retrieve the appropriate db prefix
     * @param $wpdb
     * @return mixed
     */
    public function GetDbPrefix($wpdb){
        if($this->wsal->IsMultisite()){
            return $wpdb->base_prefix;
        }
        return $wpdb->prefix;
    }

    /**
     * Validate the input from a condition
     * @param string $string
     * @return mixed
     */
    public function ValidateInput($string){
        $string = preg_replace('/<script[^>]*?>.*?<\/script>/i', '', $string);
        $string = preg_replace('/<[\/\!]*?[^<>]*?>/i', '', $string);
        $string = preg_replace('/<style[^>]*?>.*?<\/style>/i', '', $string);
        $string = preg_replace('/<![\s\S]*?--[ \t\n\r]*>/i', '', $string);
        return preg_replace("/[^a-z0-9.':\-_]/i", '', $string);
    }

    /**
     * Validate a partial IP address.
     * @param string $ip
     * @return bool
     */
    public function IsValidPartialIP($ip) {
        if( !$ip or strlen(trim($ip)) == 0){
            return false;
        }
        $ip=trim($ip);
        $parts = explode('.', $ip);
        if (count($parts) <= 4) {
            foreach ($parts as $part) {
                if ($part > 255 || $part < 0) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function GetRoleNames() {
        global $wp_roles;
        if (!isset( $wp_roles )){
            $wp_roles = new WP_Roles();
        }
        return $wp_roles->get_names();
    }

    /**
     * @internal
     * @param string $key The key to pad
     * @return string
     */
    public function PadKey($key){
        if(strlen($key) == 1){
            $key = str_pad($key, 4, '0', STR_PAD_LEFT);
        }
        return $key;
    }

    /**
     * Datetime used in the Notifications.
     */
    public function GetDatetimeFormat() {
        $date_format = $this->GetDateFormat();
        $time_format = $this->GetTimeFormat();
        return $date_format . " " . $time_format;
    }

    /**
     * Date Format from WordPress General Settings.
     */
    public function GetDateFormat() {
        return $this->wsal->settings->GetDateFormat();
    }

    /**
     * Used in the form validation.
     */
    public function DateValidFormat() {
        $search = array('Y', 'm', 'd');
        $replace = array('yyyy', 'mm', 'dd');
        return str_replace($search, $replace, $this->GetDateFormat());
    }

    /**
     * Time Format from WordPress General Settings.
     */
    public function GetTimeFormat() {
        return $this->wsal->settings->GetTimeFormat();
    }

    /**
     * Check time 24 hours.
     * @return bool true/false
     */
    public function Show24Hours() {
        $format = $this->GetTimeFormat();
        if (strpos($format, 'g') !== false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Validate a condition
     * @param object $select2
     * @param object $select3
     * @param string $inputValue
     * @return bool|int|mixed
     */
    public function ValidateCondition($select2, $select3, $inputValue)
    {
        $values = $select2->data;
        $selected = $select2->selected;

        if (!isset($values[$selected])) {
            return array('error'=>__('The form is not valid. Please reload the page and try again.', 'email-notifications-wsal'));
        }

        // Get what's selected
        $what = strtoupper($values[$selected]);
        // if ALERT CODE
        if ('ALERT CODE' == $what) {
            $length = strlen($inputValue);
            if ($length <> 4) {
                return array('error'=>__('The ALERT CODE is not valid.', 'email-notifications-wsal'));
            }
            $alerts = $this->wsal->alerts->GetAlerts();
            if (empty($alerts)) {
                return array('error'=>__('Internal Error. Please reload the page and try again.', 'email-notifications-wsal'));
            }
            // Ensure this is a valid Alert Code
            $keys = array_keys($alerts);
            $keys = array_map(array($this,'PadKey'), $keys);
            if (!in_array($inputValue, $keys)) {
                return array('error'=>__('The ALERT CODE is not valid.', 'email-notifications-wsal'));
            }
        }
        // IF USERNAME
        elseif ('USERNAME' == $what) {
            $length = strlen($inputValue);
            if ($length > 50) {
                return array('error'=>__('The USERNAME is not valid. Maximum of 50 characters allowed.', 'email-notifications-wsal'));
            }
            // make sure this is a valid username
            if (!username_exists($inputValue)) {
                return array('error'=>__('The USERNAME does not exist.', 'email-notifications-wsal'));
            }
        }
        // IF USER ROLE
        elseif ('USER ROLE' == $what) {
            $length = strlen($inputValue);
            if ($length > 50) {
                return array('error'=>__('The USER ROLE is not valid. Maximum of 50 characters allowed.', 'email-notifications-wsal'));
            }
            // Ensure this is a valid role
            $roles = $this->GetRoleNames();
            $role = strtolower($inputValue);
            if (! isset($roles[$role])) {
                return array('error'=>__('The USER ROLE does not exist.', 'email-notifications-wsal'));
            }
        }
        // IF SOURCE IP
        elseif ('SOURCE IP' == $what) {
            $length = strlen($inputValue);
            if ($length > 15) {
                return array('error'=>__('The SOURCE IP is not valid. Maximum of 15 characters allowed.', 'email-notifications-wsal'));
            }
            $val_s3 = $select3->data[$select3->selected];
            if (!$val_s3) {
                return array('error'=>__('The form is not valid. Please reload the page and try again.', 'email-notifications-wsal'));
            }
            if ('IS EQUAL' == $val_s3) {
                $r = filter_var($inputValue, FILTER_VALIDATE_IP);
                if ($r) {
                    return true;
                } else {
                    return array('error'=>__('The SOURCE IP is not valid.', 'email-notifications-wsal'));
                }
            }
            $r = $this->IsValidPartialIP($inputValue);
            if ($r) {
                return true;
            } else {
                return array('error'=>__('The SOURCE IP fragment is not valid.', 'email-notifications-wsal'));
            }
        }
        // DATE
        elseif ('DATE' == $what) {
            $date_format = $this->DateValidFormat();
            if ($date_format == 'mm-dd-yyyy' || $date_format == 'dd-mm-yyyy') {
                // regular expression to match date format mm-dd-yyyy or dd-mm-yyyy
                $regEx = '/^\d{1,2}-\d{1,2}-\d{4}$/';
            } else {
                // regular expression to match date format yyyy-mm-dd
                $regEx = '/^\d{4}-\d{1,2}-\d{1,2}$/';
            }
            $r = preg_match($regEx, $inputValue);
            if ($r) {
                return true;
            } else {
                return array('error'=>__('DATE is not valid.', 'email-notifications-wsal'));
            }
        }
        // TIME
        elseif ('TIME' == $what) {
            $timeArray = explode(':', $inputValue);
            if (count($timeArray) == 2) {
                $p1 = intval($timeArray[0]);
                if ($p1 < 0 || $p1 > 23) {
                    return array('error'=>__('TIME is not valid.', 'email-notifications-wsal'));
                }
                $p2 = intval($timeArray[1]);
                if ($p2 < 0 || $p2 > 59) {
                    return array('error'=>__('TIME is not valid.', 'email-notifications-wsal'));
                }
                return true;
            }
            return false;
        }
        // POST ID, PAGE ID, CUSTOM POST ID
        elseif ('POST ID' == $what || 'PAGE ID' == $what || 'CUSTOM POST ID' == $what) {
            $e = sprintf(__('%s is not valid', 'email-notifications-wsal'), $what);
            $inputValue = intval($inputValue);
            if (! $inputValue) {
                return array('error'=> $e);
            }
            global $wpdb;
            $t = $this->GetPostsTableName($wpdb);
            $result = $wpdb->get_var(sprintf("SELECT COUNT(ID) FROM ".$t." WHERE ID = %d", $inputValue));

            if ($result >= 1) {
                return true;
            } else {
                $e = sprintf(__('%s was not found', 'email-notifications-wsal'), $what);
                return array('error'=> $e);
            }
        }
        // SITE ID
        elseif ('SITE DOMAIN' == $what) {
            $e = sprintf(__('%s is not valid', 'email-notifications-wsal'), $what);
            if (!$inputValue) {
                return array('error'=> $e);
            }
            if ($this->wsal->IsMultisite()) {
                global $wpdb;
                $result = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM " . $wpdb->blogs. " WHERE blog_id = %s", $inputValue));
            } else {
                return array('error'=>__('The enviroment is not multisite.', 'email-notifications-wsal'));
            }
            if (!empty($result) && $result >= 1) {
                return true;
            } else {
                $e = sprintf(__('%s was not found', 'email-notifications-wsal'), $what);
                return array('error'=> $e);
            }
        }
        // POST TYPE
        elseif ('POST TYPE' == $what) {
            $e = sprintf(__('%s is not valid', 'email-notifications-wsal'), $what);
            if (!$inputValue) {
                return array('error'=> $e);
            }
            
            $length = strlen($inputValue);
            if ($length < 4) {
                return array('error'=>__('The POST TYPE is not valid. Minimum of 4 characters allowed.', 'email-notifications-wsal'));
            }
            if ($length > 15) {
                return array('error'=>__('The POST TYPE is not valid. Maximum of 15 characters allowed.', 'email-notifications-wsal'));
            }
        }
        return true;
    }

    /**
     * Retrieve a notification from the database
     * @param int $id
     * @return mixed
     */
    public function GetNotification($id)
    {
        $result = $this->wsal->GetNotification($id);
        return $result;
    }

    /**
     * Retrieve all notifications from the database
     * @param string $how
     * @return mixed
     */
    public function GetNotifications()
    {
        $result = $this->wsal->GetNotificationsSetting(WSAL_OPT_PREFIX);
        return $result;
    }

    /**
     * Check to see whether or not we can add a new notification
     * @return bool
     */
    public function CanAddNotification()
    {
        $num = $this->wsal->CountNotifications(WSAL_OPT_PREFIX);
        return $num < WSAL_MAX_NOTIFICATIONS ? true : false;
    }

    public function GetDisabledNotifications()
    {
        $notifications = $this->GetNotifications();
        
        foreach ($notifications as $i => &$entry) {
            $item = unserialize($entry->option_value);

            if ($item->status == 1) {
                unset($notifications[$i]);
                continue;
            }
        }
        $notifications = array_values($notifications);
        return $notifications;
    }

    public function GetNotBuiltInNotifications()
    {
        $notifications = $this->GetNotifications();
        
        foreach ($notifications as $i => &$entry) {
            $item = unserialize($entry->option_value);

            if (isset($item->built_in)) {
                unset($notifications[$i]);
                continue;
            }
        }
        $notifications = ($notifications) ? array_values($notifications) : null;
        return $notifications;
    }

    public function GetBuiltIn()
    {
        $notifications = $this->GetNotifications();
        $aBuilt_in = array();
        foreach ($notifications as $i => &$entry) {
            $item = unserialize($entry->option_value);

            if (isset($item->built_in)) {
                $aBuilt_in[] = $notifications[$i];
            }
        }
        return $aBuilt_in;
    }

    public function CheckBuiltInByName($name)
    {
        $name = 'wsal-notification-built-in-' . $name;
        $aBuilt_in = $this->GetBuiltIn();
        if (!empty($aBuilt_in)) {
            foreach ($aBuilt_in as $element) {
                if ($element->option_name == $name) {
                    $item = unserialize($element->option_value);
                    $checked = array();
                    foreach ($item->triggers as $value) {
                        array_push($checked, $value['input1']);
                    }
                    return array('title' => $item->title, 'email' => $item->email, 'checked' => $checked);
                }
            }
        }
        return null;
    }

    public function CheckBuiltInByType($type)
    {
        $type = 'wsal-notification-built-in-' . $type;
        $aBuilt_in = $this->GetBuiltIn();
        if (!empty($aBuilt_in)) {
            foreach ($aBuilt_in as $element) {
                if ($element->option_name == $type) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Retrieve all notifications to display in the search view
     * @param wpdb $wpdb
     * @param $search
     * @return array
     */
    public function GetSearchResults($search)
    {
        if (empty($search)) {
            return array();
        }
        $notifications = $this->GetNotifications();
        $tmp = array();
        foreach ($notifications as $entry) {
            $item = unserialize($entry->option_value);
            if (false !== ($r = stristr($item->title, $search))) {
                array_push($tmp, $entry);
                continue;
            }
        }
        return $tmp;
    }


    /**
     * JSON encode and display the Notification object in the Edit Notification view
     * @param WSAL_NP_NotificationBuilder $notifBuilder
     */
    public function CreateJsOutputEdit(WSAL_NP_NotificationBuilder $notifBuilder)
    {
        echo '<script type="text/javascript" id="wsalModelWp">';
        echo "var wsalModelWp = '".json_encode($notifBuilder->get())."';";
        echo '</script>';
    }


    /**
     * Build the js script the view will use to rebuild the form in case of an error
     * @param $notifBuilder
     */
    public function CreateJsObjOutput(WSAL_NP_NotificationBuilder $notifBuilder)
    {
        echo '<script type="text/javascript" id="wsalModelWp">';
        echo "var wsalModelWp = '".json_encode($notifBuilder->get())."';";
        echo '</script>';
    }

    public function GetNotificationsPageUrl()
    {
        $class = $this->wsal->views->FindByClassName('WSAL_NP_Notifications');
        if (false === $class) {
            $class = new WSAL_NP_Notifications($this->wsal);
        }
        return esc_attr($class->GetUrl());
    }

    /**
     * Save or update a notification into the database. This method will also validate the notification.
     * @param WSAL_NP_NotificationBuilder $notifBuilder
     * @param object $notification
     * @param wpdb $wpdb
     * @param bool $update
     * @return null|void
     */
    public function SaveNotification(WSAL_NP_NotificationBuilder $notifBuilder, $notification, $update = false)
    {
        if (!$update) {
            if (!$this->CanAddNotification()) {
                ?><div class="error"><p><?php _e('Title is required.', 'email-notifications-wsal'); ?></p></div><?php
                return $this->CreateJsObjOutput($notifBuilder);
            }
        }

        // Sanitize Title & Email
        $title = trim($notification->info->title);
        $title = str_replace(array('\\', '/'), '', $title);
        $title = sanitize_text_field($title);
        $email = trim($notification->info->email);

        // If there is Email Template
        if (!empty($notification->info->subject) && !empty($notification->info->body)) {
            // Sanitize subject and body
            $subject = trim($notification->info->subject);
            $subject = str_replace(array('\\', '/'), '', $subject);
            $subject = sanitize_text_field($subject);
            $body = $notification->info->body;
        }
        

        $notifBuilder->clearTriggersErrors();

        // Validate title
        if (empty($title)) {
            ?><div class="error"><p><?php _e('Title is required.', 'email-notifications-wsal'); ?></p></div><?php
            $notifBuilder->update('errors', 'titleMissing', __('Title is required.', 'email-notifications-wsal'));
            return $this->CreateJsObjOutput($notifBuilder);
        } else {
            $regexTitle = '/[A-Z0-9\,\.\+\-\_\?\!\@\#\$\%\^\&\*\=]/si';
            if (! preg_match($regexTitle, $title)) {
                $notifBuilder->update('errors', 'titleMissing', __('Title is not valid.', 'email-notifications-wsal'));
                return $this->CreateJsObjOutput($notifBuilder);
            }
        }

        // Set triggers
        $triggers = $notification->triggers;

        // Validate triggers
        if (empty($triggers)) {
            $notifBuilder->update('errors', 'triggersMissing', __('Please add at least one condition.', 'email-notifications-wsal'));
            return $this->CreateJsObjOutput($notifBuilder);
        }

//---------------------------------------------
// Validate conditions
//---------------------------------------------
        $hasErrors = false; // just a flag so we won't have to count notifObj->errors->triggers
        $conditions = array(); // will hold the trigger entries that will be saved into DB, so we won't have to parse the obj again
        foreach ($triggers as $i => $entry) {
            // flag
            $j = $i+1; // to help us identify the right trigger in the DOM

            // simple obj mapping
            $select1 = $entry->select1;
            $select2 = $entry->select2;
            $select3 = $entry->select3;
            $input1 = $entry->input1;
            // Checking if selected SITE DOMAIN(9)
            if ($select2->selected == 9) {
                global $wpdb;
                $input1 = $wpdb->get_var($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs WHERE domain = %s", $input1));
            }
            // Validate each trigger/condition
            if ($i) {
                // Ignore the first trigger's select1 - because it's not used
                // so we start with the second one
                // make sure the provided selected index exists in the correspondent data array
                if (! isset($select1->data[$select1->selected])) {
                    $hasErrors = true;
                    $notifBuilder->updateTriggerError($j, __('The form is not valid. Please refresh the page and try again.', 'email-notifications-wsal'));
                    continue;
                }
            }
            if (! isset($select2->data[$select2->selected])) {
                $hasErrors = true;
                $notifBuilder->updateTriggerError($j, __('The form is not valid. Please refresh the page and try again.', 'email-notifications-wsal'));
                continue;
            }
            if (! isset($select3->data[$select3->selected])) {
                $hasErrors = true;
                $notifBuilder->updateTriggerError($j, __('The form is not valid. Please refresh the page and try again.', 'email-notifications-wsal'));
                continue;
            }

            // sanitize and validate input
            $input1 = $this->ValidateInput($input1);
            $size = strlen($input1);
            if ($size > 50) {
                $hasErrors = true;
                $notifBuilder->updateTriggerError($j, __("A trigger's condition must not be longer than 50 characters.", 'email-notifications-wsal'));
                continue;
            }

            $vm = $this->ValidateCondition($select2, $select3, $input1);
            if (is_array($vm)) {
                $hasErrors = true;
                $notifBuilder->updateTriggerError($j, $vm['error']);
                continue;
            }

            // add condition
            array_push($conditions, array(
                'select1' => intval($select1->selected),
                'select2' => intval($select2->selected),
                'select3' => intval($select3->selected),
                'input1' => $input1,
            ));
        }

        // Validate email
        if (empty($email)) {
            $notifBuilder->update('errors', 'emailMissing', __('Email or Username is required.', 'email-notifications-wsal'));
            return $this->CreateJsObjOutput($notifBuilder);
        } else {
            if (!$this->CheckEmailOrUsername($email)) {
                $notifBuilder->update('errors', 'emailMissing', __('Email or Username is not valid.', 'email-notifications-wsal'));
                return $this->CreateJsObjOutput($notifBuilder);
            }
        }

        if ($hasErrors) {
            return $this->CreateJsObjOutput($notifBuilder);
        }
        // save notification
        else {
            // Build the object that will be saved into DB
            if ($update) {
                $optName = $notification->special->optName;
                // Holds the notification data that will be saved into the db
                $data = new stdClass();
                $data->title = $notification->info->title;
                $data->email = $notification->info->email;
                $data->owner = $notification->special->owner;
                $data->dateAdded = $notification->special->dateAdded;
                $data->status = $notification->special->status;
                $data->viewState = $notification->viewState;
            } else {
                $optName = WSAL_OPT_PREFIX.$this->UniqueNumber();
                // Holds the notification data that will be saved into the db
                $data = new stdClass();
                $data->title = $title;
                $data->email = $email;
                $data->owner = get_current_user_id();
                $data->dateAdded = time();
                $data->status = 1;
                $data->viewState = $notification->viewState;
            }

            // If there is Email Template
            if (!empty($subject) && !empty($body)) {
                $data->subject = $subject;
                $data->body = $body;
            }

            $data->triggers = $conditions; // this will be serialized by WP

            $result = $update ? $this->UpdateGlobalOption($optName, $data) : $this->AddGlobalOption($optName, $data);
            if ($result === false) {
                // catchy... update_option && update_site_option will both return false if one will use them to update an option
                // with the same value(s)
                // so we need to check the last error
                
                ?><div class="error"><p><?php _e('Notification could not be saved.', 'email-notifications-wsal'); ?></p></div><?php
                return $this->CreateJsObjOutput($notifBuilder);
            }
            // ALL GOOD
            ?><div class="updated"><p><?php _e('Notification successfully saved.', 'email-notifications-wsal'); ?></p></div><?php
                // send to Notifications page
                echo '<script type="text/javascript" id="wsalModelReset">';
                echo 'window.setTimeout(function(){location.href="'.$this->GetNotificationsPageUrl().'";}, 700);';
                echo '</script>';
        }
        return null;
    }

    /**
     * Email Template
     * @return array $email_body Body of the email
     */
    public function GetEmailTemplate($name, $force_default = false)
    {
        $template = array();
        $opt_name = "email-template-" . $name;
        $oTemplate = $this->GetOptionByName($opt_name);
        if (!empty($oTemplate) && !$force_default) {
            $template = json_decode(json_encode($oTemplate), true);
        } else {
            $builtInSubject = ($name == 'built-in') ? __("Built-in Notification", "email-notifications-wsal") : '';
            $default_email_subject = __(' {title} on website {site} triggered', 'email-notifications-wsal');
            $template['subject'] = $builtInSubject . $default_email_subject;

            $builtInBody = ($name == 'built-in') ? 'Built-in email notification' : 'Email Notification';
            $default_email_body = '<p>'. $builtInBody .__(' <strong>{title}</strong> was triggered. Below are the notification details:', 'email-notifications-wsal').'</p>';
            $default_email_body .= '<ul>';
            $default_email_body .= '<li>'.__('Alert ID', 'email-notifications-wsal').': {alert_id}</li>';
            $default_email_body .= '<li>'.__('Username', 'email-notifications-wsal').': {username}</li>';
            $default_email_body .= '<li>'.__('User role', 'email-notifications-wsal').': {user_role}</li>';
            $default_email_body .= '<li>'.__('IP address', 'email-notifications-wsal').': {source_ip}</li>';
            $default_email_body .= '<li>'.__('Alert Message', 'email-notifications-wsal').': {message}</li>';
            $default_email_body .= '<li>'.__('Alert generated on', 'email-notifications-wsal').': {date_time}</li>';
            $default_email_body .= '</ul>';
            $default_email_body .= '<p>'.__('Monitoring of WordPress and Email Notifications provided by <a href="http://www.wpsecurityauditlog.com">WP Security Audit Log, WordPress most comprehensive audit trail plugin</a>.', 'email-notifications-wsal').'</p>';
            $template['body'] = $default_email_body;
        }

        return $template;
    }

    public function SpecificTemplate($data = null)
    {
        ?>
        <table id="specific-template">
            <tbody class="widefat" id="email-template">
                <tr>
                    <td class="left-column">
                        <label for="columns"><h4><?php _e('Subject ', 'email-notifications-wsal'); ?></h4></label>
                    </td>
                    <td>
                        <fieldset>
                            <input class="field" type="text" name="subject" placeholder="Subject *" value="<?php echo(!empty($data['subject'])? $data['subject'] : null)?>">
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td class="left-column">
                        <label for="columns"><h4><?php _e('Body ', 'email-notifications-wsal'); ?></h4></label>
                        <span class="tags">HTML is accepted. Available template tags:<br>
                        {title} - Notification title<br>
                        {source_ip} - Client IP address<br>
                        {alert_id} - The alert code<br>
                        {date_time} - Alert generated on Date and time<br>
                        {message} - The alert message<br>
                        {username} - User login name<br>
                        {user_role} - Role(s) of the user<br>
                        {site} - Website name<br></span>
                    </td>
                    <td>
                        <fieldset>
                            <?php
                            $content = (!empty($data['body']) ? stripslashes($data['body']) : '');
                            $editor_id = 'body';
                            $settings = array('media_buttons' => false, 'editor_height' => 400);

                            wp_editor($content, $editor_id, $settings);
                            ?>
                        </fieldset>
                        <br>
                        
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    public function CounterLoginFailure($ip, $site_id, $user)
    {
        // Valid 12 hours
        $expiration = 12 * 60 * 60;

        $get_fn = $this->wsal->IsMultisite() ? 'get_site_transient' : 'get_transient';
        $set_fn = $this->wsal->IsMultisite() ? 'set_site_transient' : 'set_transient';
        if ($user) {
            $dataKnown = $get_fn(self::TRANSIENT_FAILED_COUNT);
            if (!$dataKnown) {
                $dataKnown = array();
            }
            if (!isset($dataKnown[$site_id.":".$user->ID.":".$ip])) {
                $dataKnown[$site_id.":".$user->ID.":".$ip] = 1;
            }
            $dataKnown[$site_id.":".$user->ID.":".$ip]++;
            $set_fn(self::TRANSIENT_FAILED_COUNT, $dataKnown, $expiration);
        } else {
            $dataUnknown = $get_fn(self::TRANSIENT_FAILED_UNKNOWN_COUNT);
            if (!$dataUnknown) {
                $dataUnknown = array();
            }
            if (!isset($dataUnknown[$site_id.":".$ip])) {
                $dataUnknown[$site_id.":".$ip] = 1;
            }
            $dataUnknown[$site_id.":".$ip]++;
            $set_fn(self::TRANSIENT_FAILED_UNKNOWN_COUNT, $dataUnknown, $expiration);
        }
    }

    public function IsLoginFailureLimit($limit, $ip, $site_id, $user, $exceed = false)
    {
        $get_fn = $this->wsal->IsMultisite() ? 'get_site_transient' : 'get_transient';
        $limit = ($limit+1);
        if ($user) {
            $dataKnown = $get_fn(self::TRANSIENT_FAILED_COUNT);
            if ($exceed) {
                return ($dataKnown !== false) && isset($dataKnown[$site_id.":".$user->ID.":".$ip]) && ($dataKnown[$site_id.":".$user->ID.":".$ip] > $limit);
            }
            return ($dataKnown !== false) && isset($dataKnown[$site_id.":".$user->ID.":".$ip]) && ($dataKnown[$site_id.":".$user->ID.":".$ip] == $limit);
        } else {
            $dataUnknown = $get_fn(self::TRANSIENT_FAILED_UNKNOWN_COUNT);
            if ($exceed) {
                return ($dataUnknown !== false) && isset($dataUnknown[$site_id.":".$ip]) && ($dataUnknown[$site_id.":".$ip] > $limit);
            }
            return ($dataUnknown !== false) && isset($dataUnknown[$site_id.":".$ip]) && ($dataUnknown[$site_id.":".$ip] == $limit);
        }
    }
    
    public function Counter404($site_id, $username, $ip)
    {
        // Valid 24 hours
        $expiration = 24 * 60 * 60;
        $get_fn = $this->wsal->IsMultisite() ? 'get_site_transient' : 'get_transient';
        $set_fn = $this->wsal->IsMultisite() ? 'set_site_transient' : 'set_transient';

        $data = $get_fn(self::TRANSIENT_404_COUNT);
        if (!$data) {
            $data = array();
        }
        if (!isset($data[$site_id.":".$username.":".$ip])) {
            $data[$site_id.":".$username.":".$ip] = 1;
        }
        $data[$site_id.":".$username.":".$ip]++;
        $set_fn(self::TRANSIENT_404_COUNT, $data, $expiration);
    }

    public function Is404Limit($limit, $site_id, $username, $ip, $exceed = false)
    {
        $get_fn = $this->wsal->IsMultisite() ? 'get_site_transient' : 'get_transient';
        $data = $get_fn(self::TRANSIENT_404_COUNT);
        // more than limit
        $limit = ($limit+2);
        if ($exceed) {
            return ($data !== false) && isset($data[$site_id.":".$username.":".$ip]) && ($data[$site_id.":".$username.":".$ip] > $limit);
        }
        return ($data !== false) && isset($data[$site_id.":".$username.":".$ip]) && ($data[$site_id.":".$username.":".$ip] == $limit);
    }

    public function SendNotificationEmail($email_address, $subject, $content, $alert_id)
    {
        // Get email adresses even when there is the Username
        $email_address = $this->GetEmails($email_address);
        if (WSAL_DEBUG_NOTIFICATIONS) {
            error_log("WP Security Audit Log Notification");
            error_log("Email address: ".$email_address);
            error_log("Alert ID: ".$alert_id);
        }
        $headers = "MIME-Version: 1.0\r\n";
        
        //@see: http://codex.wordpress.org/Function_Reference/wp_mail
        add_filter('wp_mail_content_type', array($this, '_set_html_content_type'));
        add_filter('wp_mail_from', array($this, 'custom_wp_mail_from'));
        add_filter('wp_mail_from_name', array($this, 'custom_wp_mail_from_name'));

        $result = wp_mail($email_address, $subject, $content, $headers);
        // Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
        remove_filter('wp_mail_content_type', array($this, '_set_html_content_type'));
        remove_filter('wp_mail_from', array($this, 'custom_wp_mail_from'));
        remove_filter('wp_mail_from_name', array($this, 'custom_wp_mail_from_name'));

        if (WSAL_DEBUG_NOTIFICATIONS) {
            error_log("Email success: ".print_r($result, true));
        }
        return $result;
    }

    public function GetTimezone()
    {
        $gmt_offset_sec = 0;
        $timezone = $this->wsal->settings->GetTimezone();
        if ($timezone) {
            $gmt_offset_sec = get_option('gmt_offset') * HOUR_IN_SECONDS;
        } else {
            $gmt_offset_sec = date('Z');
        }
        return $gmt_offset_sec;
    }

    public function GetEmailDatetime()
    {
        $date_format = $this->wsal->settings->GetDateFormat();
        $wp_time_format = get_option('time_format');
        $search = array('a', 'T', ' ');
        $replace = array('A', '', '');
        $time_format = str_replace($search, $replace, $wp_time_format);
        $gmt_offset_sec = $this->GetTimezone();

        $date_time_format = $date_format . ' @' . $time_format;
        $date = date($date_time_format, microtime(true) + $gmt_offset_sec);
        return $date;
    }

    public function GetBlogname()
    {
        if (is_multisite()) {
            $blog_id = (function_exists('get_current_blog_id') ? get_current_blog_id() : 0);
            $blogname = get_blog_option($blog_id, 'blogname');
        } else {
            $blogname = get_option('blogname');
        }
        return $blogname;
    }

    final public function _set_html_content_type()
    {
        return 'text/html';
    }

    final public function custom_wp_mail_from($original_email_from)
    {
        $email_from = $this->wsal->GetGlobalOption('from-email');
        if (!empty($email_from)) {
            return $email_from;
        } else {
            return $original_email_from;
        }
    }

    final public function custom_wp_mail_from_name($original_email_from_name)
    {
        $email_from_name = $this->wsal->GetGlobalOption('display-name');
        if (!empty($email_from_name)) {
            return $email_from_name;
        } else {
            return $original_email_from_name;
        }
    }

    /**
     * Validation email or username field
     */
    public function CheckEmailOrUsername($inputString)
    {
        $inputString = trim($inputString);
        $aEmailOrUsername = explode(",", $inputString);
        foreach ($aEmailOrUsername as $value) {
            $value = htmlspecialchars(stripslashes(trim($value)));
            // check if e-mail address is well-formed
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $user = get_user_by('login', $value);
                if (empty($user)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get email adresses by usernames
     * @return String $emails comma separated email
     */
    public function GetEmails($inputString)
    {
        $aEmails = array();
        $inputString = trim($inputString);
        $aEmailOrUsername = explode(",", $inputString);
        foreach ($aEmailOrUsername as $value) {
            $value = htmlspecialchars(stripslashes(trim($value)));
            // check if e-mail address is well-formed
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $user = get_user_by('login', $value);
                if (!empty($user)) {
                    array_push($aEmails, $user->user_email);
                }
            } else {
                array_push($aEmails, $value);
            }
        }
        return implode(",", $aEmails);
    }
}
