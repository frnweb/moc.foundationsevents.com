<?php

// Enqueue Slate CSS and JS
function slate_files() {
    wp_enqueue_style( 'slate-admin-theme', plugins_url('assets/css/slate.css', dirname(__FILE__)), array( 'acf-input' ), '1.1.7' );
    wp_enqueue_script( 'slate', plugins_url( "assets/js/slate.js", dirname(__FILE__ )), array( 'jquery' ), '1.1.7' );

    $pagetemplate = array( 'page_template' => basename(get_page_template()) );
    wp_localize_script( 'slate', 'template', $pagetemplate );
}

// Editor Styles
function slate_add_editor_styles() {
    add_editor_style( plugins_url('assets/css/editor-style.css', dirname(__FILE__)) );
}

// Actions
add_action( 'admin_enqueue_scripts', 'slate_files' );
add_action( 'login_enqueue_scripts', 'slate_files' );
add_action( 'after_setup_theme', 'slate_add_editor_styles' );



