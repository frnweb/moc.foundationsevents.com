=== Slate Admin Theme ===
Contributors: christian st. clair
Donate Link: 
Tags: wordpress admin theme, admin theme, white label, admin page, wordpress admin panel, admin, plugin, admin panel, wordpress, wordpress admin panel, flat admin theme, modern admin theme, simple admin theme, admin theme style plugin, free admin theme style plugin, backend theme, custom admin theme, new admin ui, wp admin theme, wp admin page.
Requires at least: 4.0
Tested up to: 4.7
Version: 1.1.01
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
GitHub Plugin URI: https://github.com/christianstclair/slate-plugin



== Description ==

Plugin to coincide with the Slate Theme from Foundations Recovery Network.

== Installation ==

1. Unzip `slate-plugin.zip`
2. Upload the `slate-plugin' folder to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==
[https://github.com/christianstclair/slate-plugin/blob/master/assets/images/slate-logo.png Slate-Logo]

== Changelog ==

1.1.01
- Bugfix for 1.1.0, forgot to make paths relative on the style enqueues.

1.1.0
- Restructured the organization of plugin files, complex functions can now be found in their respective php file in 'functions/'.  
- Added search.php to the functions folder. This allows the Relevanssi plugin to grab different sections on returning a search result from an ACF field. This needs to be worked out more and currently only works on Spotlights.  

1.0.70
- Added a click event to toggle the cards in a deck just like the flexible content layouts.
- Clicking on a card that is collapsed will expand it and vice versa.

1.0.69
- Putting back javascript for admin stuff.
- Fixing styles for the Deck.
- New Styles for the deck.

1.0.68
- Changed the Admin Options layout feature to only append the layout__title to the module name.
- Will no longer replace module names. 
- Additional styles for new layout__label.

1.0.67
- Styles for Query Monitor.
- Added pieces of a plugin for the ACF Fancy Repeater Field.
- deck changes for admin styles.

1.0.66
- Some styles for the Debug Bar

1.0.65
- Added some styles for the class's and id's that are now being added by the ACF Layout Filter used in the 'acf-layout.php' from slate

1.0.64
- I think I fixed the plugin updates.

1.0.63
- Trying to fix the plugin updates.

1.0.62
- Trying to fix the plugin update checker.

1.0.61
- Fixed stuff in admin-bar for ben.
- Changed a bad hover state in admin-bar.

1.0.53
- Fixed a path that was wrong.

1.0.52
- Added fallback sans-serif for typeface

1.0.51
- Made a change that fixes the 'Add Piece' Button in the wildcard flexible content field.

1.0.5
- Just changing some styles for the wp-editor.

1.0.4
- Functions: Removed a lot of unecessary functions and hooks from the templated admin-theme this plugin was based off of.

1.0.3
- Styles: changed scss styles to look more like wordpress.
- Admin Bar: changed home icon to link to a site home page and open in a new tab.

1.0.1
* Styles: Changes were made
* Admin Bar: Changed Wordpress Logo to Site Title

1.0.0
* initializing of plugin
* changed scss and divided up partials

== Upgrade Notice ==

- Yo upgrade dawg