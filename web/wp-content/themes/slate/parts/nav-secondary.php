<?php

//Start defining variables for building the header
$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');
$navsecondary = get_field('nav_secondary', 'option');


if( have_rows('nav_secondary', 'option') ) {
	// loop through all the rows of flexible content
	while ( have_rows('nav_secondary', 'option') ) { the_row() ;

		// Usernav
		if( get_row_layout() == 'usernav' ) {
			get_template_part('parts/pieces/header', 'usernav');
		}
								
		// Phone
		elseif( get_row_layout() == 'phone' ) {
			get_template_part('parts/pieces/header', 'phone');
		}

		// Search
		elseif( get_row_layout() == 'search' ) {
			get_template_part('parts/pieces/header', 'search');
		}

	}// /while have rows
}// /if have rows
?>