<?php 
					echo '<article id="post-'.get_the_ID().'"';
					echo post_class('');
					echo 'role="article" itemscope itemtype="http://schema.org/BlogPosting">';
					echo '<section class="entry-content" itemprop="articleBody">';
					echo the_content();
        			echo '<a class="button share"><span class="st_sharethis_custom">Share</span></a>';
					echo '</section>';//end section
					echo '</article>';//end article
?>