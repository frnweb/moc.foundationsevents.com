
<!-- This is the modal for the search pop over -->


<!-- Need conditional for Option 1 or Option 2 but NOT BOTH --> 




<!-- SEARCH OPTION 1 -->
	<!-- Search Modal POPUP -->
	<div id="header-search" data-toggler data-animate="fade-in fade-out">
		<?php get_search_form(); ?>
	</div><!-- /#header-search -->

	<!-- /Search Modal POPUP -->	
<!-- /SEARCH OPTION 1 -->



<!-- SEARCH OPTION 2 -->
	<!-- this is the popover option for the search -->
	<div id="header__searchform" data-reveal class="full reveal">
		<div class="inner">
		 
			<button class="close-button button--close" data-close aria-label="Close reveal" type="button">
				<span aria-hidden="true">&times;</span>
			</button>
			
			<h1>Begin searching and quickly find what you need...</h1>
			
			<?php get_search_form(); ?>
		
		</div><!-- end inner -->
	</div><!-- end header search -->
<!-- /SEARCH OPTION 2 -->