<?php

$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');

// Configure opening #usernav classes
if($navtype == 'default') {
	$userclasses = ' class="usernav usernav--default"';
}
if($navtype == 'title-bar') {
	$userclasses = ' class="usernav usernav--title-bar"';
}
if($navtype == 'stacked') {
	$userclasses = ' class="usernav usernav--stacked"';
}

// print opener
echo '<div id="usernav"'.$userclasses.'>'; ?>	

	<nav id="user-based">		
		<?php wp_nav_menu( array( 'theme_location' => 'user-links',
			'depth' => 1) ); ?>
	</nav><!-- end user-based -->

</div><!-- /#usernav -->