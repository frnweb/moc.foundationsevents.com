



	<div class="row expanded">
		<div class="columns large-12 medium-12 small-centered medium-uncentered large-uncentered">
			<div class="social-container">
				<!-- start loop for social media icons -->
				<div class="button-group">	
				    <?php
					// vars	
					$sm_icons = get_field('social_media_icons', 'option');
					$sitetitle = get_bloginfo('name');

					if( $sm_icons && in_array('facebook', $sm_icons) )// Facebook icon 
					{
					echo'<a id="facebook" href="'.get_field('facebook_link', 'option').'" class="button button--social" target="_blank" title="'.$sitetitle.' Facebook Page"><i class="fa fa-facebook" aria-hidden="true"></i></a>';
					}

					if( $sm_icons && in_array('instagram', $sm_icons) )// instagram icon 
					{
					echo'<a id="instagram" href="'.get_field('instagram_link', 'option').'" class="button button--social" target="_blank" title="'.$sitetitle.' Instagram Profile"><i class="fa fa-instagram" aria-hidden="true"></i></a>';
					}
					 
					if( $sm_icons && in_array('google', $sm_icons) )// Google icon 
					{
					echo'<a id="google" href="'.get_field('google_plus_link', 'option').'" class="button button--social" target="_blank" title="'.$sitetitle.' Google Plus Page"><i class="fa fa-google-plus" aria-hidden="true"></i></a>';
					}
					 
						 
					if( $sm_icons && in_array('pinterest', $sm_icons) )// Pinterest icon 
					{
					echo'<a id="pinterest" href="'.get_field('pinterest_link', 'option').'" class="button button--social" target="_blank" title="'.$sitetitle.' Pinterest Page"><i class="fa fa-pinterest" aria-hidden="true"></i></a>';
					} 
					 
					 if( $sm_icons && in_array('twitter', $sm_icons) )// Twitter icon 
					{
					echo'<a id="twitter" href="'.get_field('twitter_link', 'option').'" class="button button--social" target="_blank" title="'.$sitetitle.' Twitter Page"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
					}
					 
					 if( $sm_icons && in_array('youtube', $sm_icons) )// Youtube icon 
					{
					echo'<a id="youtube" href="'.get_field('youtube_link', 'option').'" class="button button--social" target="_blank" title="'.$sitetitle.' Youtube Page"><i class="fa fa-youtube" aria-hidden="true"></i></a>';
					}
					 
					  if( $sm_icons && in_array('linkedin', $sm_icons) )// LinkedIn Icon
					{
					echo'<a id="linkedin" href="'.get_field('linkedin_link', 'option').'" class="button button--social" target="_blank" title="'.$sitetitle.' LinkedIn Page"><i class="fa fa-linkedin" aria-hidden="true"></i></a>';
					}
					
					?>
				</div><!-- end the button group here -->

			</div><!-- end social container -->
		</div><!-- /.columns large-6 -->
	</div><!-- /.row expanded  nested-->