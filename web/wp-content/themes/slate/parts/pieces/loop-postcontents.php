<?php 
///this is the piece for the posts contents within the main posts page loop
					echo '<div class="post__contents">';	
					echo '<header class="post-header">';
					echo '<h2><a href="'.get_the_permalink().'" rel="bookmark">';//need to figure out how to add the title attribute
					echo the_title();
					echo '</a></h2>';
					echo '</header>';
					echo '<section class="post-excerpt" itemprop="articleBody">';
					echo the_excerpt('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); 
					echo '</section>';
					echo '</div>'; //end post__contents
?>