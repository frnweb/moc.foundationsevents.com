<!-- This is the phone number for the nav -->

<?php

$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');

// Configure opening #usernav classes
if($navtype == 'default') {
	$phoneclasses = ' class="large-2 medium-3 columns"';
}
if($navtype == 'title-bar') {
	$phoneclasses = ' class="title-bar__call"';
}
if($navtype == 'stacked') {
	$phoneclasses = ' class="large-4 medium-4 columns"';
}

// print opener
echo '<div id="callinfo"'.$phoneclasses.'>'; ?>	

	<div id="phone">
		<h3><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Desktop Header"]'); ?></h3>
	</div><!-- end #phone -->

</div><!-- #callinfo -->