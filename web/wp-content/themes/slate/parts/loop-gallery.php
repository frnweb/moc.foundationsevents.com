<?php 

$images = get_field('facility_gallery', 'option');

if( $images ): ?>
    <div class="orbit" role="region" aria-label="<?php bloginfo('name'); ?>" data-orbit>
        <ul class="orbit-container">
            <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fa fa-caret-left" aria-hidden="true"></i></button><button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fa fa-caret-right" aria-hidden="true"></i></button>

            <?php foreach( $images as $image ): ?>
                <li class="is-active orbit-slide">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="orbit-image"/>
                </li>
            <?php endforeach; ?>
        </ul><!-- end orbit container -->
    </div><!--- end orbit slider -->
  
    
<?php endif; ?>
