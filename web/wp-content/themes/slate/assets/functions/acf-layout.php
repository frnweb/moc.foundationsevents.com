<?php
// This file handles the admin area and functions - You can use this file to make changes to the dashboard.

function acf_admin_head_layout() {

    ?>

    <style type="text/css">

        /* Remove borders on li since we not removing li's  */
        .acf-fc-popup li {
            border:0 !important;
        }

        /* WordPress Template "home"
         * - hide ACF layouts named "block__quote", "block__text-img" and "block__list"
         */
        .has-template-home .acf-fc-popup a[data-layout="the_content"],
        .has-template-home .wp-editor-expand {     
            display: none;
        }

        /* WordPress Landing template
         */
        .has-template-landing .acf-fc-popup a[data-layout="billboard_clone"] {
            display: none;
        }

        /* WordPress default template
         * - hide ACF layout named "billboard_clone"
         */
        .has-template-default .acf-fc-popup a[data-layout="billboard_clone"],
        .has-template-default .acf-fc-popup a[data-layout="the_content"]{
            display: none;
        }

    </style>

    <script type="text/javascript">

        (function($) {

            
            $(document).ready(function(){

                <?php 

                global $post;
                
                // Set a javascript variabel "$template_name" based on selected template in WP
                // The variable is then used in window.UpdateACFView to add a CSS class
                // name to the body-tag. Since standard/custom post types doesn't have 
                // page templates we need to set this variable on the first page load
                
                if ($post->post_type == "faq") : // set 'post' for standard post
                    echo 'var $template_name = "post-type/faq";';                   
                else :
                    // Just get the template name
                    echo 'var $template_name = $("#page_template").val();';
                endif; 
                
                ?>

                // Add classes to body 
                window.UpdateACFView = function($template_name) {

                    if ($template_name == "template-home.php") {   
                        $('body').addClass('has-template-home');                            
                    }

                    if ($template_name == "default") {   
                        $('body').addClass('has-template-default');                            
                    }

                    if ($template_name == "template-full-width.php") {   
                        $('body').addClass('has-template-landing');                            
                    }

                }
                
                window.UpdateACFView($template_name); 

            });

            // When a user change the template in the dropdown we need to remove all our custom classes on the body-tag
            // and then, when ajax is completed, trigger window.UpdateACFView in order to set the correct body-class
            $(document).on('change', '#page_template', function(){

                var $template_name = $('#page_template').val();

                $(document).on('ajaxComplete', function(){
                    $('body').removeClass('has-template-home has-template-default has-template-landing'); // Clear all custom classes
                    window.UpdateACFView($template_name); 
                    $(document).off('ajaxComplete');
                });
            });
            
        })(jQuery);  

    </script>  

    <?php
}
add_action('acf/input/admin_head', 'acf_admin_head_layout');


// ACF Layout Title Options
function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
  
  // load sub field image
  $image = get_sub_field('layout_image');
  $text = get_sub_field('layout_title');
  // module class/id
  $addclass = get_sub_field('add_moduleclass');
  $addid = get_sub_field('add_moduleid');
  $class = get_sub_field('module_class');
  $id = get_sub_field('module_id');

  
  // load text sub field
  if($text) {
    // remove layout title from text
    $title = '<h4 class="layout__label">'.$layout['label'].'</h4>';
    $title .= '<h4 class="layout__title"> -  ' . $text . '</h4>';
    
  }
  if($image) {
    $title .= '<div class="layout__img thumbnail">';
    $title .= '<img src="' . $image['sizes']['thumbnail'] . '" height="36px" />';
    $title .= '</div>';
  }

  // Add the id and/or class to the module row
  if($class or $id) {
    $title .= '<p class="layout__info">';
    if($id) {
        $title .= '<span class="info--id">id</span>="'.$id.'"';
    }
    if($class) {
        $title .= ' <span class="info--class">class</span>="'.$class.'"';
    }
    $title .= '</p>';
  }
  
  // return
  return $title;
}

// name
add_filter('acf/fields/flexible_content/layout_title/name=flexible-content', 'my_acf_flexible_content_layout_title', 10, 4);
add_filter('acf/fields/flexible_content/layout_title/name=wildcard_content', 'my_acf_flexible_content_layout_title', 10, 4);



// Make Repeater Fields Collapsible despite not selecting a collapsed field to show
add_filter('acf/load_field/name=deck', 'make_it_collapse');
add_filter('acf/load_field/name=carousel_repeater', 'make_it_collapse');
  function make_it_collapse($field) {
    $field['collapsed'] = 'field_whatever';
    return $field;
  }


// Formatting For Modals
function load_field_buttonselect($field) {
  // Adding choices to the select for button targets
  $field['choices']['modal'] = 'Modal';
  return $field;
}
add_filter('acf/load_field/name=button_target_select', 'load_field_buttonselect');
add_filter('acf/load_field/name=link_select', 'load_field_buttonselect');
// /Formatting For Modals



?>