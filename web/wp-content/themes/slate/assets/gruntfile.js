module.exports = function(grunt) {

  // Configure task(s)
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),


    // Uglify JS
    uglify: {
      dev: {
        options: {
          mangle: true,
          compress: true,
        },
        src: [
          'js/viewportchecker.js',
          'js/modernizr-custom.js',
          'js/jquery.bxslider-rahisified.js',
          'js/google-maps.js',
          'js/scripts.js',
          '!js/*.min.js',
          '!js/custom-bxsliders.js'
        ],
        dest: 'js/slate.min.js'
      }
    },// /Uglify


    //autoprefix
    postcss: {
      options: {
        map: false,
        processors: [
          require('autoprefixer')({browsers: ['last 10 version', '> 10%', '> 5% in US', 'ie 8', 'ie 7']})
        ]
      },
      dist: {
        src: 'css/style.beautified.css'
      }
    },// /autoprefix
    

    // cssmin
    cssmin: {
      options: {
        restructuring: false,
        shorthandCompacting: false,
        roundingPrecision: -1,
        sourceMap: true
      },

      dev: {
        files: {
          'css/slate.min.css': ['css/*.css', '!css/*.min.css']
        }
      }
    },// /cssmin


    // Watch
    watch: {
      js: {
        files: ['js/*.js', '!js/*.min.js'],
        tasks: ['uglify:dev']
      },

    autoprefix: {
        files: 'css/style.beautified.css',
        tasks: ['postcss']
      },

      css: {
        files: ['css/*.css', '!css/*.min.css'],
        tasks: ['cssmin:dev']
      }

      
    }// /watch

  });




  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');//minifies the JS
  grunt.loadNpmTasks('grunt-contrib-watch');//compiles the JS
  grunt.loadNpmTasks('grunt-contrib-cssmin');// this compiles and minifies CSS
  grunt.loadNpmTasks('grunt-postcss');// this is for the autoprefixer

  // Register task(s).
  grunt.registerTask('default', ['uglify:dev', 'cssmin:dev', 'watch']);

};