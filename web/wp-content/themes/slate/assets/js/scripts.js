jQuery(document).foundation();
/*
These functions make sure WordPress
and Foundation play nice together.
*/

$(document).ready(function() {
	"use strict"; 
    // Remove empty P tags created by WP inside of Accordion and Orbit
    $('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	$('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
  $('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( $(this).innerWidth() / $(this).innerHeight() > 1.5 ) {
      $(this).wrap("<div class='widescreen flex-video'/>");
    } else {
      $(this).wrap("<div class='flex-video'/>");
    }
  });

});

//this is for touch devices with the megamenu 
$(document).ready(function() {
	"use strict"; 
	$('.header #top-bar-menu ul.mega-menu li.has-dropdown-arrow a.mm_link').on("touchstart", function (e) {
    var link = $(this); //preselect the link
    if (link.hasClass('hover')) {
        return true;
		
    } else {
        link.addClass("hover");
		link.parent("li").children("div.mega-menu-wrapper").addClass("showmenu");
        $('.header #top-bar-menu ul.mega-menu li.has-dropdown-arrow a.mm_link').not(this).removeClass("hover");
		$('.header #top-bar-menu ul.mega-menu li.has-dropdown-arrow a.mm_link').not(this).parent("li").children("div.mega-menu-wrapper").removeClass("showmenu");
        e.preventDefault();
        return false; //extra, and to make sure the function has consistent return points
    }
	});
});




// Foundations Fix Top-Bar - Opens-Right
$(document).ready(function() {
  $('.is-dropdown-submenu-parent.opens-left').each(function() {
    $(this).removeClass("opens-left");
    $(this).addClass("opens-right");
  });
});
//


$(document).ready(function() {
    //this animates the phone number in at the top on mobile
	"use strict";
    var mphone = $("#mobilephone");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 105) {
            mphone.removeClass('relative').addClass("fixed animated slideInDown");
        } else {
            mphone.removeClass("fixed animated slideInDown").addClass('relative');
        }
    });
});

$(document).ready(function() {
    //this animates the scroll back to the top arrow on the site
	"use strict";
    var mphone = $("#backtop");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 105) {
            mphone.removeClass('hidden').addClass("visible animated FadeInUp");
        } else {
            mphone.removeClass("visible animated FadeInUp").addClass('hidden');
        }
    });
});



//smooth scroll for the back to top button 
$(document).ready(function() {
	"use strict";
  $('a#backtop[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


// Hamburger Menu Icon
$(document).ready(function(){
  
  var $hamburger = $("button.hamburger");
  
  $('[data-toggle="off-canvas"], .js-off-canvas-exit').on('click', function(e) {
      $hamburger.toggleClass("is-active");
      // Do something else, like open/close menu
  });

  $('[data-toggle="popover"]').on('click', function(e) {
      $hamburger.toggleClass("is-active");
      // Do something else, like open/close menu
  });

});



