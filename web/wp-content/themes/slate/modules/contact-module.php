<?php
/*
Module: Contact
*/
?>

<?php
$optionheader = get_sub_field('optional_header');
$optionheadertext = get_sub_field('optional_header_text');
$mapview = get_sub_field('map_view');
$mapwidth = get_sub_field('map_width');
$cardwidth = (12 - $mapwidth);

$contactimage = get_sub_field('image_url');
$addressselect = get_sub_field('address_select');
$contactheader = get_sub_field('contact_header');

$altphonecheck = get_sub_field('altphone_check');
$altphonetext = get_sub_field('altphone_text');
$altphonenumber = get_sub_field('altphone_number');

$addresstext = apply_filters('the_content', get_sub_field('address_text'));
$button = get_field('contact_page_button','option');
$buttontarget = $button['button_target'];
$buttontext = $button['button_text'];

$location = get_sub_field('the_map');
$popupheader = get_sub_field('popup_header');
$popupselect = get_sub_field('popup_select');
$popupaddresstext = get_sub_field('popup_address_text');
$popupaddbutton = get_sub_field('popup_add_button');
$popupbuttontext = get_sub_field('popup_button_text');
$popupbuttontarget = get_sub_field('popup_button_target');
?>


<div class="module contact">
	<div class="inner expanded">
	
		<?php
		// Module Header
		if ($optionheader) { ?>
			<div class="module__header">
				<h1><?php echo $optionheadertext ?></h1>
			</div><!-- end module header -->	 	
		<?php 
		}// /if optionheader ?>

	 	<div class="row expanded collapse" data-equalizer data-equalize-on="medium">

	 		<?php 

	 		// MAP REPEATER
	 		if($mapview == "left") {
			echo '<div class="contact__map large-'.$mapwidth.' medium-'.$mapwidth.' columns">';
			}

			if($mapview == "right") {
			echo '<div class="contact__map large-'.$mapwidth.' medium-'.$mapwidth.' large-push-'.$cardwidth.' medium-push-'.$cardwidth.' columns">';
			} ?>

		 		<?php if( have_rows('map_repeater') ): ?>

					<div class="acf-map" data-equalizer-watch>
						<?php while ( have_rows('map_repeater') ) : the_row();
							$location = get_sub_field('the_map');
							$popupheader = get_sub_field('popup_header');
							$popupselect = get_sub_field('popup_select');
							$popupaddresstext = get_sub_field('popup_address_text');
							$popupaddbutton = get_sub_field('popup_add_button');
							$popupbuttontext = get_sub_field('popup_button_text');
							$popupbuttontarget = get_sub_field('popup_button_target');
						 ?>
						<!-- START POPUP -->
						<div class="contact__marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
							
							<div class="contact__popup">
								<?php
								// Header
								if ($popupheader) {
									echo '<h2>'.$popupheader.'</h2>';
								}
								// Address
								if ($popupselect == "facility_address") { ?>
									
									<!-- Address -->
									<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			                            <span itemprop="streetAddress"><?php the_field('street_address', 'option'); ?></span> <br />
			                            <span itemprop="addressLocality"><?php the_field('city_state', 'option'); ?></span> 
			                            <span itemprop="postalCode"><?php the_field('zip_code', 'option'); ?></span>
		                     		</p>
		                     		
		                     		
		                     		    <p>

                                  <span itemprop="telephone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Custom module Popup"]'); ?></span>

                                  <?php

                                  if ($altphonecheck) {
                                    echo 'or <span itemprop="telephone">';

                                      echo '<a href="tel:'.$altphonenumber.'">'.$altphonetext.'</a>';

                                    echo '</span>';
                                  }
                                  ?>
                                </p>

								<?php
								}

								// Custom Address
								if ($popupselect == "custom_address") {
									echo '<p>'.$popupaddresstext.'</p>';
								}
								
								// Button
								if ($popupaddbutton) {
									echo '<a class="button" href="'.$popupbuttontarget.'" target="_blank">'.$popupbuttontext.'</a>';
								} ?>


							</div><!-- /.contact__popup -->
						</div><!-- /.contact__marker -->
						<!-- END POPUP -->
				<?php endwhile; ?>
					</div><!-- /.acf-map -->
				<?php endif; ?>

			</div><!-- /.contact__map -->


			
			<?php

			// CONTACT CONTENT
			if($mapview == "left") {
			 	echo '<div class="large-'.$cardwidth.' medium-'.$cardwidth.' columns contact__content" itemscope itemtype="http://schema.org/Organization" data-equalizer-watch>';
				}

			if($mapview == "right") {
				echo '<div class="large-'.$cardwidth.' medium-'.$cardwidth.' large-pull-'.$mapwidth.' medium-pull-'.$mapwidth.' columns contact__content" itemscope itemtype="http://schema.org/Organization" data-equalizer-watch>';
				}
					
					if ($contactimage) { ?>
						<div class="contact__img" style="background-image: url(<?php echo $contactimage ?>);" >

					 	</div><!-- end contact__img -->
				 	<?php } ?>
	
					<!-- Content -->
				 	<div class="card card--contact">
				 		
						<?php
				 		// Header
				 		echo '<h1 itemprop="name" class="card__header-contact">'.$contactheader.'</h1>';

						// Facility Address
						if ($addressselect == "facility") { ?>
	                    	<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	                            <span itemprop="streetAddress"><?php the_field('street_address', 'option'); ?></span> <br />
	                            <span itemprop="addressLocality"><?php the_field('city_state', 'option'); ?></span> 
	                            <span itemprop="postalCode"><?php the_field('zip_code', 'option'); ?></span>
	                     	</p>
	                    <?php 
	                    // Phone Numbers
						echo '<p><span itemprop="telephone">'.do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Contact Module"]').'</span>';

							// Alt Phone Number
							if ($altphonecheck) {
							  echo ' or <span itemprop="telephone">';

							    echo '<a href="tel:'.$altphonenumber.'" onClick="ga("send", "event", "phone", "alternate phone number");">'.$altphonetext.'</a>';

							  echo '</span>';
							}
						echo '</p>';// /Phone Numbers
	                	}// /Facility Address


	                	// Custom Address
	                    elseif ($addressselect == "custom") {
							echo $addresstext;
						}// /elseif "custom"

						
						


	                    ?>

				 	</div>
				 	<!-- end card--contact -->


				</div><!-- // /.contact__content.columns -->

		</div><!-- end .row -->
	</div><!-- end .inner -->
</div><!-- end .module contact-->






