<?php
/*
Module: Content
*/
?>


<?php
	// module class
	$addclass = get_sub_field('add_moduleclass');
	$class = get_sub_field('module_class');


// If Module Class
if ($addclass) {
echo '<div class="module wysiwyg--content '.$class.'">';
} else {
echo '<div class="module wysiwyg--content">';
}// /If Module Class ?>

	<div class="inner">
	<div class="entry-content">

		<?php the_content(); ?>			
	
	</div><!-- end entry-content -->
	</div><!-- end inner -->
</div><!-- end module -->