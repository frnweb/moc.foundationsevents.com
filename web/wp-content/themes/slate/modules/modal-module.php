<?php

// Modals for Buttons

// Class/ID
$addclass = get_sub_field('add_moduleclass');
$class = get_sub_field('module_class');
$id = get_sub_field('module_id');

// Modal Fields
$modaltype = get_sub_field('modal_type');
$modalvideo = get_sub_field('modal_video');


// MODAL
echo '<div id="'.$id.'" class="reveal modal'.$class.'" data-reveal data-close-on-click="true" data-animation-in="scale-in-down" data-animation-out="fade-out" data-reset-on-close="true">';

	// CONTENT
	if($modaltype === 'content') {
		get_template_part('modules/wildcard', 'card');
	}

	// VIDEO
	if($modaltype === 'video') {

		// VIDEO iframe
		echo $modalvideo
		?>
		
		

	<?php } ?>

	<button class="close-button" data-close aria-label="Close modal" type="button">
		<span aria-hidden="true">&times;</span>
	</button><!-- /Close button -->

</div><!-- /.reveal.modal -->

