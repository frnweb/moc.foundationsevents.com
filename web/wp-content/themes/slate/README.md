![SlateLogo_Outline.png](https://bitbucket.org/repo/Gk7LpE/images/2586853517-SlateLogo_Outline.png)

### Version 1.8.27 ###

**Code/Bug Changes**  
- Added the Wrapper feature to the wildcard Card.  
- Added the function that returns the classes and module info in the admin options to the wildcard flexible content field.  


See the full [CHANGELOG](https://bitbucket.org/frnweb/slate/src/fd53868392f76f1ea94b78a17a98fd18236ebc3c/CHANGELOG.md?at=master&fileviewer=file-view-default)


*Currently using Foundation 6.2.1 and Advanced Custom Fields Pro 5.5.3

### What is SLATE?

SLATE is a modular and scalable wordpress framework built on Foundations 6 and Advanced Custom Fields. SLATE runs on a Child/Parent concept meaning that to customize your site you MUST use a child theme. SLATE edits are made in one central repository, therefore changes should not be made within the core files. 

Download the child theme from the SLATE Child theme base to begin customizing some sites.

### What else do I need to know about SLATE? 
Slate is built from the Joints WP starter theme. JointsWP comes pre-baked with all of the great features that are found in the Foundation framework – simply put, if it works in Foundation, it will work in JointsWP. The theme also includes:

* Foundation Navigation Options
* Motion-UI
* Grid archive templates
* Translation Support
* Bower and Gulp Support
* And much, much more!