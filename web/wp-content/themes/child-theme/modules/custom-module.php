<div id="" class="module wysiwyg dark-angle-background hide-for-medium">
    <div class="inner">
        <div class="entry-content">
            <h1>Agenda</h1>
            <ul class="tabs " data-tabs id="agenda-tabs">
                <li class="tabs-title  " role="presentation"><a href="#sundaypanel" role="tab" aria-controls="sundaypanel" aria-selected="false" id="sundaypanel-label"><strong>Sunday</strong><br>OCT 1</a></li>
                <li class="tabs-title  is-active" role="presentation"><a href="#mondaypanel" role="tab" aria-controls="mondaypanel" aria-selected="true" id="mondaypanel-label"><strong>Day 1</strong><br>OCT 2</a></li>
                <li class="tabs-title  " role="presentation"><a href="#tuesdaypanel" role="tab" aria-controls="tuesdaypanel" aria-selected="false" id="tuesdaypanel-label"><strong>Day 2</strong><br>OCT 3</a></li>
                <li class="tabs-title  " role="presentation"><a href="#wednesdaypanel" role="tab" aria-controls="wednesdaypanel" aria-selected="false" id="wednesdaypanel-label"><strong>Day 3</strong><br>OCT 4</a></li>
                <li class="tabs-title  " role="presentation"><a href="#thursdaypanel" role="tab" aria-controls="thursdaypanel" aria-selected="false" id="thursdaypanel-label"><strong>Final Day</strong><br>OCT 5</a></li>
            </ul><!-- agenda tabs ul -->
            <div class="tabs-content" data-tabs-content="agenda-tabs">
                <div class="tabs-panel " id="sundaypanel" role="tabpanel" aria-hidden="true" aria-labelledby="sundaypanel-label">
                            <h1><strong>Sunday</strong></h1><h1>OCT 1</h1>
                            <div class="container panel">
                            <div class="container half">
                                <p><strong>Exhibitor Setup</strong><br>1:00 PM - 6:00 PM</p>
                                <p><strong>Early Registration</strong><br>1:00 PM - 7:30 PM</p>
                                <p><strong>Welcome Reception</strong><br>6:30 PM - 8:30 PM</p>
                            </div><!--container half -->
                            </div><!-- container panel -->
                    </div><!--tabs panel -->
                <div class="tabs-panel " id="mondaypanel" role="tabpanel" aria-hidden="false" aria-labelledby="mondaypanel-label">
                    <h1><strong>Day 1</strong></h1><h1>OCT 2</h1>
                    <ul class="tabs " data-tabs id="monday-tabs">
                        <li class="tabs-title is-active" role="presentation"><a href="#mondaymorning" role="tab" aria-controls="mondaymorning" aria-selected="false" id="mondaymorning-label">Morning</a></li>
                        <li class="tabs-title" role="presentation"><a href="#mondayafternoon" role="tab" aria-controls="mondayafternoon" aria-selected="true" id="mondayafternoon-label">Afternoon</a></li>
                    </ul><!-- monday tabs ul -->
                    <div class="tabs-content" data-tabs-content="monday-tabs">
                        <div class="tabs-panel " id="mondaymorning" role="tabpanel" aria-hidden="true" aria-labelledby="mondaymorning-label">
                            <div class="container half ">
                                <p><strong>Yoga</strong><br>6:30 AM - 7:30 AM</p>
                                <p><strong>Motivational Run</strong><br>6:30 AM - 7:30 AM</p>
                                <p><strong>Open Meeting</strong><br>7:00 AM - 8:00 AM</p>
                                <p><strong>Registration</strong><br>7:30 AM - 5:00 PM</p>
                                <p><strong>Breakfast</strong><br>7:30 AM - 8:30 AM</p>
                                <p><strong>Exhibit Hall Open</strong><br>7:30 AM - 5:00 PM</p>
                            </div><!--container half -->
                            <div class="container half ">
                                <p><strong>Steve Ford</strong><br>8:30 AM - 10:00 AM</p>
                                <p><strong>Break/Networking</strong><br>10:00 AM - 10:30 AM</p>
                                <p><strong>Heroes Award</strong><br>10:00 AM - 10:15 AM</p>
                                <p><strong>Breakout Sessions</strong><br>10:30 AM - 12:00 PM</p>
                            </div><!--container half -->
                        </div><!--tabs panel monday morning -->
                        <div class="tabs-panel " id="mondayafternoon" role="tabpanel" aria-hidden="true" aria-labelledby="mondayafternoon-label">    
                            <div class="container half ">
                                <p><strong>Lunch</strong><br>12:00 PM - 1:30 PM</p>
                                <p><strong>Breakout Sessions</strong><br>1:30 PM - 3:00 PM</p>
                                <p><strong>Break/Networking</strong><br>3:00 PM - 3:30 PM</p>
                                <p><strong>Judy Crane</strong><br>3:30 PM - 5:00 PM</p>
                                <p><strong>Open Meeting</strong><br>6:00 PM - 7:00 PM</p>
                                <p><strong>Recovery Yoga</strong><br>6:00 PM - 7:00 PM</p>
                            </div><!--container half -->
                            <div class="container half ">
                                <p><strong>Evening Event</strong><br>6:30 PM - 8:30 PM</p>
                            </div><!--container half -->
                        </div><!--tabs panel monday afternoon -->
                    </div><!--monday tabs content -->
                    </div><!--monday tabs panel -->
                <div class="tabs-panel " id="tuesdaypanel" role="tabpanel" aria-hidden="true" aria-labelledby="tuesdaypanel-label">
                    <h1><strong>Day 2</strong></h1><h1>OCT 3</h1>
                    <ul class="tabs " data-tabs id="tuesday-tabs">
                        <li class="tabs-title is-active" role="presentation"><a href="#tuesdaymorning" role="tab" aria-controls="tuesdaymorning" aria-selected="false" id="tuesdaymorning-label">Morning</a></li>
                        <li class="tabs-title" role="presentation"><a href="#tuesdayafternoon" role="tab" aria-controls="tuesdayafternoon" aria-selected="true" id="tuesdayafternoon-label">Afternoon</a></li>
                    </ul><!-- tuesday tabs ul -->
                    <div class="tabs-content" data-tabs-content="tuesday-tabs">
                        <div class="tabs-panel " id="tuesdaymorning" role="tabpanel" aria-hidden="true" aria-labelledby="tuesdaymorning-label">
                            <div class="container half ">
                                <p><strong>Yoga</strong><br>6:30 AM - 7:30 AM</p>
                                <p><strong>Motivational Run</strong><br>6:30 AM - 7:30 AM</p>
                                <p><strong>Open Meeting</strong><br>7:00 AM - 8:00 AM</p>
                                <p><strong>Registration</strong><br>7:30 AM - 5:00 PM</p>
                                <p><strong>Breakfast</strong><br>7:30 AM - 8:30 AM</p>
                                <p><strong>Exhibit Hall Open</strong><br>7:30 AM - 5:00 PM</p>
                            </div><!--container half -->
                            <div class="container half ">
                                <p><strong>Joseph Troncale</strong><br>8:30 AM - 10:00 AM</p>
                                <p><strong>Break/Networking</strong><br>10:00 AM - 10:30 AM</p>
                                <p><strong>Breakout Sessions</strong><br>10:30 AM - 12:00 PM</p>
                            </div><!--container half -->
                        </div> <!-- tabs panel tuesday morning -->
                        <div class="tabs-panel " id="tuesdayafternoon" role="tabpanel" aria-hidden="true" aria-labelledby="tuesdayafternoon-label">
                        <div class="container half ">
                            <p><strong>Lunch</strong><br>12:00 PM - 1:30 PM</p>
                            <p><strong>Breakout Sessions</strong><br>1:30 PM - 3:00 PM</p>
                            <p><strong>Break/Networking</strong><br>3:00 PM - 3:30 PM</p>
                            <p><strong>Brad Sylvain & Megan Van Ella</strong><br>3:30 PM - 5:00 PM</p>
                            <p><strong>Open Meeting</strong><br>6:00 PM - 7:00 PM</p>
                            <p><strong>Yoga</strong><br>6:00 PM - 7:00 PM</p>
                        </div><!--container half -->
                        <div class="container half ">
                        </div><!--container half -->
                        </div><!-- tabs panel tuesday afternoon -->
                        </div><!-- tuesday tabs content -->
            </div><!-- tuesday tabs panel -->
            <div class="tabs-panel " id="wednesdaypanel" role="tabpanel" aria-hidden="true" aria-labelledby="wednesdaypanel-label">
                <h1><strong>Day 3</strong></h1><h1>OCT 4</h1>
                <ul class="tabs " data-tabs id="wednesday-tabs">
                        <li class="tabs-title is-active" role="presentation"><a href="#wednesdaymorning" role="tab" aria-controls="wednesdaymorning" aria-selected="false" id="wednesdaymorning-label">Morning</a></li>
                        <li class="tabs-title" role="presentation"><a href="#wednesdayafternoon" role="tab" aria-controls="wednesdayafternoon" aria-selected="true" id="wednesdayafternoon-label">Afternoon</a></li>
                    </ul><!-- wednesday tabs ul -->
                    <div class="tabs-content" data-tabs-content="wednesday-tabs">
                        <div class="tabs-panel " id="wednesdaymorning" role="tabpanel" aria-hidden="true" aria-labelledby="wednesdaymorning-label">
                        <div class="container half ">
                            <p><strong>Yoga</strong><br>6:30 AM - 7:30 AM</p>
                            <p><strong>Motivational Run</strong><br>6:30 AM - 7:30 AM</p>
                            <p><strong>Open Meeting</strong><br>7:00 AM - 8:00 AM</p>
                            <p><strong>Registration</strong><br>7:30 AM - 5:00 PM</p>
                            <p><strong>Breakfast</strong><br>7:30 AM - 8:30 AM</p>
                            <p><strong>Exhibit Hall Open</strong><br>7:30 AM - 5:00 PM</p>
                        </div><!--container half -->
                        <div class="container half ">
                            <p><strong>Douglas Tieman & Brad Sorte</strong><br>8:30 AM - 10:00 AM</p>
                            <p><strong>Break/Networking</strong><br>10:00 AM - 10:30 AM</p>
                            <p><strong>Breakout Sessions</strong><br>10:30 AM - 12:00 PM</p>
                        </div><!--container half -->
                        </div><!-- wednesday morning panel -->
                    <div class="tabs-panel " id="wednesdayafternoon" role="tabpanel" aria-hidden="true" aria-labelledby="wednesdayafternoon-label">
                        <div class="container half ">
                            <p><strong>Lunch</strong><br>12:00 PM - 1:30 PM</p>
                            <p><strong>Breakout Sessions</strong><br>1:30 PM - 3:00 PM</p>
                            <p><strong>Break/Networking</strong><br>3:00 PM - 3:30 PM</p>
                            <p><strong>Breakout Sessions</strong><br>3:30 PM - 5:00 PM</p>
                            <p><strong>Open Meeting</strong><br>6:00 PM - 7:00 PM</p>
                            <p><strong>Yoga</strong><br>6:00 PM - 7:00 PM</p>
                        </div><!--container half -->
                        <div class="container half ">
                        </div><!--container half -->
                    </div><!-- wednesday afternoon panel -->
                    </div><!-- wednesday tabs content -->
            </div><!-- Wednesday tabs panel -->
            <div class="tabs-panel " id="thursdaypanel" role="tabpanel" aria-hidden="true" aria-labelledby="thursdaypanel-label">
                <h1><strong>Final Day</strong></h1><h1>OCT 5</h1>
                        <div class="container panel">
                        <div class="container half">
                            <p><strong>Yoga</strong><br>6:30 AM - 7:30 AM</p>
                            <p><strong>Motivational Run</strong><br>6:30 AM - 7:30 AM</p>
                            <p><strong>Open Meeting</strong><br>7:00 AM - 8:00 AM</p>
                            <p><strong>Registration</strong><br>7:30 AM - 10:30 AM</p>
                            <p><strong>Breakfast</strong><br>7:30 AM - 8:30 AM</p>
                        </div><!--container half -->
                        <div class="container half">
                            <p><strong>Exhibit Hall Open</strong><br>7:30 AM - 10:30 AM</p>
                            <p><strong>Breakout Sessions</strong><br>8:30 AM - 10:00 AM</p>
                            <p><strong>Break/Networking</strong><br>10:00 AM - 10:30 AM</p>
                            <p><strong>John Southworth, Jay Schneider, Caroline Smith, Jeff Jay & Rob Waggener</strong><br>10:30 AM - 12:00 PM</p>
                        </div><!--container half -->
                        </div><!-- container panel -->
            </div><!-- thursday tabs panel -->
            </div><!--agenda tabs content -->
	</div><!-- end entry content -->	
    </div><!-- end inner  -->
</div><!-- end module wysiwyg -->