"use strict"

import gulp from "gulp"
import plugins from "gulp-load-plugins"
import yaml from "js-yaml"
import fs from "fs"
import yargs from "yargs"
import rimraf from "rimraf"
import browser from "browser-sync"
import webpack2 from "webpack"
import webpackStream from "webpack-stream"
import named from "vinyl-named"
import git from "gulp-git"

// Load all Gulp plugins into one variable
const $ = plugins()

let PRODUCTION = !!yargs.argv.production

// Load settings from config.yml
const { PORT, PROXY, COMPATIBILITY, PATHS } = loadConfig()

function loadConfig() {
  let ymlFile = fs.readFileSync("config.yml", "utf8")
  return yaml.load(ymlFile)
}

// Build the 'dist' folder by running all of the tasks below.
gulp.task(
  "build",
  gulp.series(clean, gulp.parallel(sass, images, javascript, copy))
)

// Build the site, run the server, and watch for changes.
gulp.task("default", gulp.series("build", server, watch))

// Run a production build and add it to every git commit
gulp.task(
  "pre-commit",
  gulp.series(switchToProduction, "build", addDistToCommit)
)

function switchToProduction(done) {
  PRODUCTION = true
  done()
}

function addDistToCommit() {
  return gulp.src("./dist").pipe(git.add({ cwd: "../../../../" }))
}

// Delete the 'dist' folder.
// This happens everytime a build starts
function clean(done) {
  rimraf(PATHS.dist, done)
}

// Copy files out of the assets directory.
// This task skips over the img, js, and scss folders,
// which are parsed separately.
function copy() {
  return gulp.src(PATHS.assets).pipe(gulp.dest(PATHS.dist + "/assets"))
}

// Compile Sass into CSS.
// In production, CSS is compressed
function sass() {
  return gulp
    .src("src/assets/scss/main.scss")
    .pipe($.sourcemaps.init())
    .pipe($.sass({ includePaths: PATHS.sass }).on("error", $.sass.logError))
    .pipe($.autoprefixer({ browsers: COMPATIBILITY }))
    .pipe($.if(PRODUCTION, $.cleanCss({ compatability: "ie9" })))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + "/assets/css"))
    .pipe(browser.reload({ stream: true }))
}

let webpackConfig = {
  module: {
    rules: [
      {
        test: /.js$/,
        use: [
          {
            loader: "babel-loader"
          }
        ]
      }
    ]
  }
}

function javascript() {
  return gulp
    .src(PATHS.entries)
    .pipe(named())
    .pipe($.sourcemaps.init())
    .pipe(webpackStream(webpackConfig, webpack2))
    .pipe(
      $.if(
        PRODUCTION,
        $.uglify().on("error", e => {
          console.log(e)
        })
      )
    )
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + "/assets/js"))
}

// Copy images to the 'dist' folder
// In production, the images are compressed
function images() {
  return gulp
    .src("src/assets/img/**/*")
    .pipe(
      $.if(
        PRODUCTION,
        $.imagemin({
          progressive: true
        })
      )
    )
    .pipe(gulp.dest(PATHS.dist + "/assets/img"))
}

// Start a server with Browsersync to preview the site in
function server(done) {
  browser.init({ proxy: PROXY, port: PORT })
  done()
}

// Reload the browser with Browsersync
function reload(done) {
  browser.reload()
  done()
}

// Watch for changes to static assets, php files, sass, and javascript
function watch() {
  gulp.watch(PATHS.assets, copy)
  gulp.watch("**/*.php").on("all", gulp.series(browser.reload))
  gulp
    .watch("src/assets/js/**/*.js")
    .on("all", gulp.series(javascript, browser.reload))
  gulp.watch("src/assets/scss/**/*.scss").on("all", sass)
  gulp
    .watch("src/assets/img/**/*")
    .on("all", gulp.series(images, browser.reload))
}
